(** This file extends the proof mode with support for reasoning about fractional
predicates. It does so by declaring appropriate [IntoX] and [FromX] instances
for all of the connectives.

The key idea of the proof mode instances in this file is to make sure that one
introduces or eliminates a logical connective, the fractions are pushed through.
Let's consider a goal:

<<<
((P1 ∧ P2) → (∃ x, Q x)) π
>>>

Writing `iIntros "H"` would turn this into:

<<<
H : P1 π ∧ P2 π
---------------∗
(∃ x, Q x π)
>>>

In the case the `P`s and `Q`s contain other logical connectives hereditarily,
the fraction are of course pushed in further.

CORE INVARIANT: In order to make reasoning with fractional predicates readable
in the proof mode output, the proof mode class instances in this file try to
uphold the following invariant:

- Fractions are always pushed through maximally for all logical operators but
  implications, wands, and forall quantifiers.

The correct functioning of all tactics relies on this invariant. If the
invariant does not hold, the tactics will fail. For example, `iDestruct`
will not work if a hypothesis is of the shape `(P1 ∧ P2) π`. The normalization
of fractions should already have been done when that hypothesis was introduced
into the context.

The pushing through of fractions is done by the type class [MakeFracPredAt];
which should have instances for all logical connectives but implication, wand,
and forall. As a result of that, special instances for the [IntoX] and [FromX]
classes are only needed for implication, wand, and forall.

This file is mostly a copy of `proofmode/monpred.v` in the Iris repository,
where a similar construction is carried out for monotone predicates over a
partial order. There is one difference however:

- We rely on the above invariant to always hold, and tactics fail if the
  invariant does not.
- The monpred proofmode extension in Iris tries to mitigate this problem. When
  a hypothesis is not of the right shape (because the user managed to break
  the invariant by fiddling around with non proofmode tactics manually, e.g.
  rewrite) they still try to do something.

The instances of monpred that tried to manage the latter are still left as
comments in this file. *)
From iron.bi Require Export fracpred.
From iris.bi Require Import plainly.
From iris.proofmode Require Import proofmode modality_instances.

Class MakeFracPredAt {PROP : bi} (π : option frac) (P : fracPred PROP) (PP : PROP) :=
  make_fracPred_at : P π ⊣⊢ PP.
Arguments MakeFracPredAt {_} _ _%_I _%_I.
Global Hint Mode MakeFracPredAt + - ! - : typeclass_instances.

Section bi.
Context {PROP : bi}.
Local Notation fracPredI := (fracPredI PROP).
Local Notation fracPred := (fracPred PROP).
Local Notation MakeFracPredAt := (@MakeFracPredAt PROP).
Implicit Types P Q R : fracPred.
Implicit Types PP QQ RR : PROP.
Implicit Types φ : Prop.

Global Instance from_modal_affinely_fracPred_at φ `(sel : A) P Q QQ :
  FromModal φ modality_affinely sel P Q → MakeFracPredAt ε Q QQ →
  FromModal φ modality_affinely sel (P ε) QQ | 0.
Proof.
  rewrite /FromModal /MakeFracPredAt /==> HPQ <- ?. rewrite -HPQ //.
  by rewrite fracPred_at_affinely bi.pure_True // left_id.
Qed.
Global Instance from_modal_persistently_fracPred_at φ `(sel : A) P Q QQ :
  FromModal φ modality_persistently sel P Q → MakeFracPredAt ε Q QQ →
  FromModal φ modality_persistently sel (P ε) QQ | 0.
Proof.
  rewrite /FromModal /MakeFracPredAt /==> HPQ <- ?. by rewrite -HPQ // fracPred_at_persistently.
Qed.
Global Instance from_modal_intuitionistically_fracPred_at φ `(sel : A) P Q QQ :
  FromModal φ modality_intuitionistically sel P Q → MakeFracPredAt ε Q QQ →
  FromModal φ modality_intuitionistically sel (P ε) QQ | 0.
Proof.
  rewrite /FromModal /MakeFracPredAt /==> HPQ <- ?. rewrite -HPQ //.
  by rewrite fracPred_at_affinely fracPred_at_persistently bi.pure_True // left_id.
Qed.
Global Instance from_modal_id_fracPred_at φ `(sel : A) P Q QQ π :
  FromModal φ modality_id sel P Q → MakeFracPredAt π Q QQ →
  FromModal φ modality_id sel (P π) QQ.
Proof. rewrite /FromModal /MakeFracPredAt=> HPQ <- ?. by rewrite -HPQ //. Qed.

Global Instance make_fracPred_at_pure φ π : MakeFracPredAt π ⌜φ⌝ ⌜φ⌝.
Proof. by rewrite /MakeFracPredAt fracPred_at_pure. Qed.
Global Instance make_fracPred_at_emp π : MakeFracPredAt π emp (<affine> ⌜π = ε⌝).
Proof. by rewrite /MakeFracPredAt fracPred_at_emp. Qed.
Global Instance make_fracPred_at_sep π P Q (Φ Ψ : option frac → PROP) :
  (∀ π, MakeFracPredAt π P (Φ π)) →
  (∀ π, MakeFracPredAt π Q (Ψ π)) →
  MakeFracPredAt π (P ∗ Q) (∃ π1 π2, ⌜ π = π1 ⋅ π2 ⌝ ∧ Φ π1 ∗ Ψ π2).
Proof.
  rewrite /MakeFracPredAt fracPred_at_sep=> H1 H2.
  by setoid_rewrite <-H1; setoid_rewrite <-H2.
Qed.
Global Instance make_fracPred_at_and π P PP Q QQ :
  MakeFracPredAt π P PP → MakeFracPredAt π Q QQ →
  MakeFracPredAt π (P ∧ Q) (PP ∧ QQ).
Proof. by rewrite /MakeFracPredAt fracPred_at_and=><-<-. Qed.
Global Instance make_fracPred_at_or π P PP Q QQ :
  MakeFracPredAt π P PP → MakeFracPredAt π Q QQ →
  MakeFracPredAt π (P ∨ Q) (PP ∨ QQ).
Proof. by rewrite /MakeFracPredAt fracPred_at_or=><-<-. Qed.
Global Instance make_fracPred_at_forall {A} π (Φ : A → fracPred) (Ψ : A → PROP) :
  (∀ a, MakeFracPredAt π (Φ a) (Ψ a)) → MakeFracPredAt π (∀ a, Φ a) (∀ a, Ψ a).
Proof. rewrite /MakeFracPredAt fracPred_at_forall=>H. by setoid_rewrite <- H. Qed.
Global Instance make_fracPred_at_exists {A} π (Φ : A → fracPred) (Ψ : A → PROP) :
  (∀ a, MakeFracPredAt π (Φ a) (Ψ a)) → MakeFracPredAt π (∃ a, Φ a) (∃ a, Ψ a).
Proof. rewrite /MakeFracPredAt fracPred_at_exist=>H. by setoid_rewrite <- H. Qed.
Global Instance make_fracPred_at_persistently π P PP :
  MakeFracPredAt ε P PP → MakeFracPredAt π (<pers> P) (<pers> PP).
Proof. by rewrite /MakeFracPredAt fracPred_at_persistently=><-. Qed.
Global Instance make_fracPred_at_affinely π P PP :
  MakeFracPredAt ε P PP →
  MakeFracPredAt π (<affine> P) (<affine> (⌜π = ε⌝ ∧ PP)).
Proof. by rewrite /MakeFracPredAt fracPred_at_affinely=><-. Qed.
Global Instance make_fracPred_at_intuitionistically π P PP :
  MakeFracPredAt ε P PP → MakeFracPredAt π (□ P) (□ (⌜π = ε⌝ ∧ PP)).
Proof. by rewrite /MakeFracPredAt fracPred_at_intuitionistically=><-. Qed.
Global Instance make_fracPred_at_absorbingly π P Φ :
  (∀ π, MakeFracPredAt π P (Φ π)) →
  MakeFracPredAt π (<absorb> P) (<absorb> (∃ π', ⌜ π' ≼ π ⌝ ∧ Φ π')).
Proof.
  rewrite /MakeFracPredAt fracPred_at_absorbingly=> H. by setoid_rewrite <-H.
Qed.
Global Instance make_fracPred_at_embed π PP : MakeFracPredAt π ⎡PP⎤ PP.
Proof. by rewrite /MakeFracPredAt fracPred_at_embed. Qed.
Global Instance make_fracPred_at_default π P : MakeFracPredAt π P (P π) | 100.
Proof. by rewrite /MakeFracPredAt. Qed.
Global Instance make_fracPred_at_bupd `{BiBUpd PROP} π P PP :
  MakeFracPredAt π P PP → MakeFracPredAt π (|==> P)%I (|==> PP)%I.
Proof. by rewrite /MakeFracPredAt fracPred_at_bupd=> <-. Qed.

(* LEFT OVER from monpred, SEE ABOVE:

Global Instance from_assumption_make_fracPred_at_l p π P PP :
  MakeFracPredAt π P PP → KnownLFromAssumption p (P π) PP.
Proof.
  rewrite /MakeFracPredAt /KnownLFromAssumption /FromAssumption=><-.
  apply bi.intuitionistically_if_elim.
Qed.
Global Instance from_assumption_make_fracPred_at_r p π P PP :
  MakeFracPredAt π P PP → KnownRFromAssumption p PP (P π).
Proof.
  rewrite /MakeFracPredAt /KnownRFromAssumption /FromAssumption=><-.
  apply bi.intuitionistically_if_elim.
Qed. *)

Global Instance as_emp_valid_fracPred_at φ P PP :
  AsEmpValid0 φ P → MakeFracPredAt ε P PP → AsEmpValid φ PP | 100.
Proof.
  rewrite /MakeFracPredAt /AsEmpValid0 /AsEmpValid /bi_emp_valid=>-> <-. split.
  - move=>[H]. rewrite -H fracPred_at_emp bi.pure_True //.
    by rewrite bi.affinely_True_emp.
  - move=>HP. split=>π. rewrite fracPred_at_emp /bi_affinely HP.
    by apply bi.pure_elim_r=> ->.
Qed.
Global Instance as_emp_valid_fracPred_at_wand φ P Q Φ Ψ :
  AsEmpValid0 φ (P -∗ Q) →
  (∀ π, MakeFracPredAt π P (Φ π)) → (∀ π, MakeFracPredAt π Q (Ψ π)) →
  AsEmpValid φ (∀ π, Φ π -∗ Ψ π).
Proof.
  rewrite /AsEmpValid0 /AsEmpValid /MakeFracPredAt. intros -> EQ1 EQ2.
  setoid_rewrite <-EQ1. setoid_rewrite <-EQ2. split.
  - move=>/bi.wand_entails HP. setoid_rewrite HP. by iIntros (π) "$".
  - move=>HP. apply bi.entails_wand. split=>π. iIntros "H". by iApply HP.
Qed.
Global Instance as_emp_valid_fracPred_at_equiv φ P Q Φ Ψ :
  AsEmpValid0 φ (P ∗-∗ Q) →
  (∀ π, MakeFracPredAt π P (Φ π)) → (∀ π, MakeFracPredAt π Q (Ψ π)) →
  AsEmpValid φ (∀ π, Φ π ∗-∗ Ψ π).
Proof.
  rewrite /AsEmpValid0 /AsEmpValid /MakeFracPredAt. intros -> EQ1 EQ2.
  setoid_rewrite <-EQ1. setoid_rewrite <-EQ2. split.
  - move=>/bi.wand_iff_equiv HP. setoid_rewrite HP. iIntros. iSplit; iIntros "$".
  - move=>HP. apply bi.equiv_wand_iff. split=>π. by iSplit; iIntros; iApply HP.
Qed.

(* LEFT OVER from monpred, SEE ABOVE:

Global Instance into_pure_fracPred_at P φ π : IntoPure P φ → IntoPure (P π) φ.
Proof. rewrite /IntoPure=>->. by rewrite fracPred_at_pure. Qed.
Global Instance from_pure_fracPred_at_false P φ π :
  FromPure false P φ → FromPure false (P π) φ.
Proof. rewrite /FromPure /= =><-. by rewrite fracPred_at_pure. Qed.
Global Instance from_pure_fracPred_at_true P φ :
  FromPure true P φ → FromPure true (P ε) φ.
Proof.
  rewrite /FromPure /= =><-. rewrite !fracPred_at_affinely fracPred_at_pure.
  by rewrite (bi.pure_True (_ = _)) // left_id.
Qed.

Global Instance into_persistent_fracPred_at_true P Q QQ :
  IntoPersistent true P Q → MakeFracPredAt ε Q QQ → IntoPersistent true (P ε) QQ | 0.
Proof.
  rewrite /IntoPersistent /MakeFracPredAt=>-[/(_ ε) H] <-. revert H.
  by rewrite /= !fracPred_at_persistently.
Qed.
Global Instance into_persistent_fracPred_at_false P Q QQ π :
  IntoPersistent false P Q → MakeFracPredAt ε Q QQ → IntoPersistent false (P π) QQ | 0.
Proof.
  rewrite /IntoPersistent /MakeFracPredAt=>-[/(_ π) H] <-. revert H.
  by rewrite /= !fracPred_at_persistently.
Qed. *)

Global Instance into_wand_fracPred_at p q R P PP Q QQ π π' :
  TCOr (TCEq p false) (TCEq π ε) →
  TCOr (TCEq q false) (TCEq π' ε) →
  IntoWand p q R P Q → MakeFracPredAt π' P PP → MakeFracPredAt (π ⋅ π') Q QQ →
  IntoWand p q (R π) PP QQ.
Proof.
  rewrite /IntoWand /MakeFracPredAt /bi_affinely_if /bi_persistently_if.
  intros Hπ Hπ' H <- <-. destruct p; simpl in *.
  - destruct Hπ as [Hπ| ->]; first by inversion Hπ.
    move: H=> [/(_ ε)]. rewrite fracPred_at_intuitionistically bi.pure_True // left_id.
    intros ->. apply bi.wand_intro_l. rewrite fracPred_at_wand.
    destruct q; simpl in *.
    + destruct Hπ' as [Hπ'| ->]; first by inversion Hπ'.
      rewrite (bi.forall_elim ε) fracPred_at_intuitionistically.
      by rewrite bi.pure_True // left_id bi.wand_elim_r.
    + by rewrite (bi.forall_elim π') bi.wand_elim_r.
  - rewrite H. apply bi.wand_intro_l. rewrite fracPred_at_wand.
    destruct q; simpl in *.
    + destruct Hπ' as [Hπ'| ->]; first by inversion Hπ'.
      rewrite (bi.forall_elim ε) fracPred_at_intuitionistically.
      by rewrite bi.pure_True // left_id bi.wand_elim_r.
    + by rewrite (bi.forall_elim π') bi.wand_elim_r.
Qed.

(* LEFT OVER from monpred, SEE ABOVE:
Global Instance into_wand_wand'_fracPred p q P Q PP QQ π :
  IntoWand' p q ((P -∗ Q) π) PP QQ → IntoWand p q ((P -∗ Q) π) PP QQ | 100.
Proof. done. Qed.
Global Instance into_wand_impl'_fracPred p q P Q PP QQ π :
  IntoWand' p q ((P → Q) π) PP QQ → IntoWand p q ((P → Q) π) PP QQ | 100.
Proof. done. Qed. *)

(* [π] is the default name for the fraction *)
Global Instance from_forall_fracPred_at_wand R P Q Φ Ψ π :
  FromWand R P Q →
  (∀ π', MakeFracPredAt π' P (Φ π')) → (∀ π', MakeFracPredAt π' Q (Ψ π')) →
  FromForall (R π) (λ π', Φ π' -∗ Ψ (π ⋅ π'))%I (to_ident_name π).
Proof.
  rewrite /FromWand /FromForall /MakeFracPredAt=> <- H1 H2.
  rewrite fracPred_at_wand. do 2 f_equiv. by rewrite H1 H2.
Qed.
Global Instance from_impl_fracPred_at_impl R P Q PP QQ π :
  FromImpl R P Q →
  MakeFracPredAt π P PP → MakeFracPredAt π Q QQ →
  FromImpl (R π) PP QQ.
Proof.
  rewrite /FromImpl /MakeFracPredAt=> <- <- <-. by rewrite fracPred_at_impl.
Qed.

(* LEFT OVER from monpred, SEE ABOVE:
Global Instance from_and_fracPred_at P Q1 QQ1 Q2 QQ2 π :
  FromAnd P Q1 Q2 → MakeFracPredAt π Q1 QQ1 → MakeFracPredAt π Q2 QQ2 →
  FromAnd (P π) QQ1 QQ2.
Proof.
  rewrite /FromAnd /MakeFracPredAt /MakeFracPredAt=> <- <- <-.
  by rewrite fracPred_at_and.
Qed.
Global Instance into_and_fracPred_at_false P Q1 QQ1 Q2 QQ2 π :
  IntoAnd false P Q1 Q2 → MakeFracPredAt π Q1 QQ1 → MakeFracPredAt π Q2 QQ2 →
  IntoAnd false (P π) QQ1 QQ2.
Proof.
  rewrite /IntoAnd /MakeFracPredAt /= => -> <- <-. by rewrite fracPred_at_and.
Qed.
Global Instance into_and_fracPred_at_true P Q1 QQ1 Q2 QQ2 :
  IntoAnd true P Q1 Q2 → MakeFracPredAt ε Q1 QQ1 → MakeFracPredAt ε Q2 QQ2 →
  IntoAnd true (P ε) QQ1 QQ2.
Proof.
  rewrite /IntoAnd /MakeFracPredAt /= => -[/(_ ε) H] <- <-; revert H.
  rewrite !fracPred_at_intuitionistically bi.pure_True // !left_id.
  by rewrite fracPred_at_and bi.intuitionistically_and.
Qed.

Global Instance from_or_fracPred_at P Q1 QQ1 Q2 QQ2 π :
  FromOr P Q1 Q2 → MakeFracPredAt π Q1 QQ1 → MakeFracPredAt π Q2 QQ2 →
  FromOr (P π) QQ1 QQ2.
Proof. rewrite /FromOr /MakeFracPredAt=> <- <- <-. by rewrite fracPred_at_or. Qed.
Global Instance into_or_fracPred_at P Q1 QQ1 Q2 QQ2 π :
  IntoOr P Q1 Q2 → MakeFracPredAt π Q1 QQ1 → MakeFracPredAt π Q2 QQ2 →
  IntoOr (P π) QQ1 QQ2.
Proof. rewrite /IntoOr /MakeFracPredAt=> -> <- <-. by rewrite fracPred_at_or. Qed.

Global Instance from_exist_fracPred_at {A} P (Φ : A → fracPred) (Ψ : A → PROP) π :
  FromExist P Φ → (∀ a, MakeFracPredAt π (Φ a) (Ψ a)) → FromExist (P π) Ψ.
Proof.
  rewrite /FromExist /MakeFracPredAt=><- H. setoid_rewrite <- H.
  by rewrite fracPred_at_exist.
Qed.
Global Instance into_exist_fracPred_at {A} P (Φ : A → fracPred) (Ψ : A → PROP) π :
  IntoExist P Φ → (∀ a, MakeFracPredAt π (Φ a) (Ψ a)) → IntoExist (P π) Ψ.
Proof.
  rewrite /IntoExist /MakeFracPredAt=>-> H. setoid_rewrite <- H.
  by rewrite fracPred_at_exist.
Qed. *)

Global Instance from_forall_fracPred_at {A} P (Φ : A → fracPred) (Ψ : A → PROP) π name :
  FromForall P Φ name → (∀ a, MakeFracPredAt π (Φ a) (Ψ a)) →
  FromForall (P π) Ψ name.
Proof.
  rewrite /FromForall /MakeFracPredAt=><- H. setoid_rewrite <- H.
  by rewrite fracPred_at_forall.
Qed.
Global Instance into_forall_fracPred_at {A} P (Φ : A → fracPred) (Ψ : A → PROP) π :
  IntoForall P Φ → (∀ a, MakeFracPredAt π (Φ a) (Ψ a)) → IntoForall (P π) Ψ.
Proof.
  rewrite /IntoForall /MakeFracPredAt=>-> H. setoid_rewrite <- H.
  by rewrite fracPred_at_forall.
Qed.

(* LEFT OVER from monpred, SEE ABOVE:
Global Instance frame_fracPred_at p P Q QQ R i j :
  IsBiIndexRel i j → Frame p R P Q → MakeFracPredAt j Q QQ →
  Frame p (R i) (P j) QQ.
Proof.
  rewrite /Frame /MakeFracPredAt /bi_affinely_if /bi_persistently_if
          /IsBiIndexRel=> Hij <- <-.
  destruct p; by rewrite Hij fracPred_at_sep ?fracPred_at_affinely ?fracPred_at_persistently.
Qed.
Global Instance frame_fracPred_at_embed π p P Q QQ RR :
  Frame p ⎡RR⎤ P Q → MakeFracPredAt π Q QQ → Frame p RR (P π) QQ.
Proof.
  rewrite /Frame /MakeFracPredAt /bi_affinely_if /bi_persistently_if=> <- <-.
  destruct p; by rewrite fracPred_at_sep ?fracPred_at_affinely
                         ?fracPred_at_persistently fracPred_at_embed.
Qed.

Global Instance into_embed_objective P :
  FObjective P → IntoEmbed P (∀ i, P i).
Proof.
  rewrite /IntoEmbed=> ?.
  by rewrite {1}(objective_objectively P) fracPred_objectively_unfold.
Qed.

Global Instance elim_modal_at_bupd_goal `{BiBUpd PROP} φ p p' PP PP' Q Q' i :
  ElimModal φ p p' PP PP' (|==> Q i) (|==> Q' i) →
  ElimModal φ p p' PP PP' ((|==> Q) i) ((|==> Q') i).
Proof. by rewrite /ElimModal !fracPred_at_bupd. Qed.
Global Instance elim_modal_at_bupd_hyp `{BiBUpd PROP} φ p p' P PP PP' QQ QQ' π:
  MakeFracPredAt π P PP →
  ElimModal φ p p' (|==> PP) PP' QQ QQ' →
  ElimModal φ p p' ((|==> P) π) PP' QQ QQ'.
Proof. by rewrite /MakeFracPredAt /ElimModal fracPred_at_bupd=><-. Qed.

Global Instance add_modal_at_bupd_goal `{BiBUpd PROP} φ PP PP' Q π :
  AddModal PP PP' (|==> Q π) → AddModal PP PP' ((|==> Q) π).
Proof. by rewrite /AddModal !fracPred_at_bupd. Qed. *)

(* Higher precendence than elim_modal_bupd *)
Global Instance elim_modal_bupd_affine `{BiAffine PROP, BiBUpd PROP} P P' Q :
  FromAffinely P' P →
  ElimModal True true false (|==> P) P' (|==> Q) (|==> Q) | 0.
Proof.
  rewrite /FromAffinely /ElimModal /= => <- _.
  rewrite bi.intuitionistically_affinely fracPred_affinely_bupd.
  by rewrite bupd_frame_r bi.wand_elim_r bupd_trans.
Qed.
Global Instance elim_modal_embed_affine_bupd_hyp `{BiAffine PROP, BiBUpd PROP}
    p p' φ PP P' Q Q' :
  ElimModal φ p p' (|==> <affine> ⎡PP⎤)%I P' Q Q' →
  ElimModal φ p p' (<affine> ⎡|==> PP⎤) P' Q Q'.
Proof. by rewrite /ElimModal embed_bupd -fracPred_affinely_bupd. Qed.

(* LEFT OVER from monpred, SEE ABOVE:
(* When P and/or Q are evars when doing typeclass search on [IntoWand
   (R i) P Q], we use [MakeFracPredAt] in order to normalize the
   result of unification. However, when they are not evars, we want to
   propagate the known information through typeclass search. Hence, we
   do not want to use [MakeFracPredAt].

   As a result, depending on P and Q being evars, we use a different
   version of [into_wand_fracPred_at_xx_xx]. *)
Hint Extern 3 (IntoWand _ _ (fracPred_at _ _) ?P ?Q) =>
     is_evar P; is_evar Q;
     eapply @into_wand_fracPred_at_unknown_unknown
     : typeclass_instances.
Hint Extern 2 (IntoWand _ _ (fracPred_at _ _) ?P (fracPred_at ?Q _)) =>
     eapply @into_wand_fracPred_at_unknown_known
     : typeclass_instances.
Hint Extern 2 (IntoWand _ _ (fracPred_at _ _) (fracPred_at ?P _) ?Q) =>
     eapply @into_wand_fracPred_at_known_unknown_le
     : typeclass_instances.
Hint Extern 2 (IntoWand _ _ (fracPred_at _ _) (fracPred_at ?P _) ?Q) =>
     eapply @into_wand_fracPred_at_known_unknown_ge
     : typeclass_instances.
*)

(* LEFT OVER from monpred, SEE ABOVE:
Global Instance from_forall_fracPred_at_plainly `{BiPlainly PROP} i P Φ :
  (∀ π, MakeFracPredAt π P (Φ π)) →
  FromForall ((■ P) π) (λ j, ■ (Φ j))%I.
Proof.
  rewrite /FromForall /MakeFracPredAt=>HPΦ. rewrite fracPred_at_plainly.
  by setoid_rewrite HPΦ.
Qed.
Global Instance into_forall_fracPred_at_plainly `{BiPlainly PROP} i P Φ :
  (∀ i, MakeFracPredAt π P (Φ i)) →
  IntoForall ((■ P) i) (λ j, ■ (Φ j))%I.
Proof.
  rewrite /IntoForall /MakeFracPredAt=>HPΦ. rewrite fracPred_at_plainly.
  by setoid_rewrite HPΦ.
Qed.

Global Instance is_except_0_fracPred_at π P :
  IsExcept0 P → IsExcept0 (P π).
Proof. rewrite /IsExcept0=>- [/(_ π)]. by rewrite fracPred_at_except_0. Qed.
*)

Global Instance make_fracPred_at_internal_eq `{!BiInternalEq PROP} {A : ofe} (x y : A) π :
  MakeFracPredAt π (x ≡ y) (x ≡ y).
Proof. by rewrite /MakeFracPredAt fracPred_at_internal_eq. Qed.
Global Instance make_fracPred_at_except_0 π P QQ :
  MakeFracPredAt π P QQ → MakeFracPredAt π (◇ P) (◇ QQ).
Proof. by rewrite /MakeFracPredAt fracPred_at_except_0=><-. Qed.
Global Instance make_fracPred_at_later π P QQ :
  MakeFracPredAt π P QQ → MakeFracPredAt π (▷ P) (▷ QQ).
Proof. by rewrite /MakeFracPredAt fracPred_at_later=><-. Qed.
Global Instance make_fracPred_at_laterN π n P QQ :
  MakeFracPredAt π P QQ → MakeFracPredAt π (▷^n P) (▷^n QQ).
Proof. rewrite /MakeFracPredAt=> <-. elim n=>//= ? <-. by rewrite fracPred_at_later. Qed.

(* LEFT OVER from monpred, SEE ABOVE:
Global Instance make_fracPred_at_fupd `{BiFUpd PROP} π E1 E2 P PP :
  MakeFracPredAt π P PP → MakeFracPredAt π (|={E1,E2}=> P) (|={E1,E2}=> PP).
Proof. by rewrite /MakeFracPredAt fracPred_at_fupd=> <-. Qed.

Global Instance into_internal_eq_fracPred_at {A : ofe} (x y : A) P π :
  IntoInternalEq P x y → IntoInternalEq (P π) x y.
Proof. rewrite /IntoInternalEq=> ->. by rewrite fracPred_at_internal_eq. Qed.

Global Instance into_except_0_fracPred_at_fwd π P Q QQ :
  IntoExcept0 P Q → MakeFracPredAt π Q QQ → IntoExcept0 (P π) QQ.
Proof. rewrite /IntoExcept0 /MakeFracPredAt=> -> <-. by rewrite fracPred_at_except_0. Qed.
Global Instance into_except_0_fracPred_at_bwd π P PP Q :
  IntoExcept0 P Q → MakeFracPredAt π P PP → IntoExcept0 PP (Q π).
Proof. rewrite /IntoExcept0 /MakeFracPredAt=> H <-. by rewrite H fracPred_at_except_0. Qed.

Global Instance maybe_into_later_fracPred_at π n P Q QQ :
  IntoLaterN false n P Q → MakeFracPredAt π Q QQ →
  IntoLaterN false n (P π) QQ.
Proof.
  rewrite /IntoLaterN /MaybeIntoLaterN /MakeFracPredAt=> -> <-. elim n=>//= ? <-.
  by rewrite fracPred_at_later.
Qed.
Global Instance from_later_fracPred_at π `(sel : A) n P Q QQ :
  FromModal (modality_laterN n) sel P Q → MakeFracPredAt π Q QQ →
  FromModal (modality_laterN n) sel (P π) QQ.
Proof.
  rewrite /FromModal /MakeFracPredAt=> <- <-. elim n=>//= ? ->.
  by rewrite fracPred_at_later.
Qed.

Global Instance elim_modal_at_fupd_goal `{BiFUpd PROP} φ p p' E1 E2 E3 PP PP' Q Q' i :
  ElimModal φ p p' PP PP' (|={E1,E3}=> Q i) (|={E2,E3}=> Q' i) →
  ElimModal φ p p' PP PP' ((|={E1,E3}=> Q) i) ((|={E2,E3}=> Q') i).
Proof. by rewrite /ElimModal !fracPred_at_fupd. Qed.
Global Instance elim_modal_at_fupd_hyp `{BiFUpd PROP} φ p p' E1 E2 P PP PP' QQ QQ' i :
  MakeFracPredAt π P PP →
  ElimModal φ p p' (|={E1,E2}=> PP) PP' QQ QQ' →
  ElimModal φ p p' ((|={E1,E2}=> P) i) PP' QQ QQ'.
Proof. by rewrite /MakeFracPredAt /ElimModal fracPred_at_fupd=><-. Qed.

Global Instance add_modal_at_fupd_goal `{BiFUpd PROP} E1 E2 PP PP' Q i :
  AddModal PP PP' (|={E1,E2}=> Q i) → AddModal PP PP' ((|={E1,E2}=> Q) i).
Proof. by rewrite /AddModal !fracPred_at_fupd. Qed.

Global Instance elim_inv_embed φ PPinv PPin PPout PPclose Pin Pout Pclose Q Q' :
  (∀ i, ElimInv φ PPinv PPin PPout PPclose (Q i) (Q' i)) →
  MakeEmbed PPin Pin → MakeEmbed PPout Pout → MakeEmbed PPclose Pclose →
  ElimInv φ ⎡PPinv⎤ Pin Pout Pclose Q Q'.
Proof.
  rewrite /MakeEmbed /ElimInv=>H <- <- <- ?. iStartProof PROP.
  iIntros (?) "(?&?&HQ')". iApply H; [done|]. iFrame. iIntros "?". by iApply "HQ'".
Qed.
*)
End bi.
