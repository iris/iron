(** This file proves the basic and correct usage of resources adequacy
statements for [heap_lang]. It does do so by appealing to the generic results
in [iron_logic/adequacy].

Note, we only state adequacy for the lifted logic, because in the Coq
formalization we state all specifications in terms of the lifted logic. *)
From iris.algebra Require Import big_op gmap ufrac excl ufrac_auth.
From iron.iron_logic Require Export weakestpre adequacy.
From iron.heap_lang Require Import heap.
From iris.proofmode Require Import proofmode.
Set Default Proof Using "Type".

Class heapGpreS Σ := HeapGpreS {
  heap_preG_iris :: ironInvPreG Σ;
  heap_preG_inG :: inG Σ (ufrac_authR heapUR);
  heap_preG_fork_post_inG :: inG Σ (authR (gmapUR positive (exclR (optionO ufracO))));
  heap_preG_proph :: proph_mapGpreS proph_id (val * val) Σ;
}.

Definition heapΣ : gFunctors := #[
  ironInvΣ;
  GFunctor (ufrac_authR heapUR);
  GFunctor (authR (gmapUR positive (exclR (optionO ufracO))));
  proph_mapΣ proph_id (val * val)].
Global Instance subG_heapPreG {Σ} : subG heapΣ Σ → heapGpreS Σ.
Proof. solve_inG. Qed.

(** A generic helper *)
Theorem heap_adequacy Σ `{heapGpreS Σ} s e σ φ :
  (∀ `{!heapGS Σ},
    ([∗ map] l ↦ v ∈ omap id σ.(heap), l ↦ v) -∗
    WP e @ s; ⊤ {{ v,
      |={⊤,∅}=> ⎡∀ σ' m, heap_ctx (omap id σ'.(heap)) m -∗ ⌜φ v σ'⌝⎤ }}) →
  adequate s e σ φ.
Proof.
  (* TODO: refactor this proof so we don't have the two cases *)
  destruct (decide (omap id σ.(heap) = ∅)) as [Hempty|Heq].
  - intros Hwp; eapply (iron_wp_adequacy _ _ _ _ _ _ (1 / 2) ε); iIntros (? κs) "".
    iMod (own_alloc (●U_1 ∅ ⋅ (◯U_(1/2) ∅ ⋅ ◯U_(1/2) ε))) as (γ) "[Hσ [Hp Hp']]".
    { rewrite -ufrac_auth_frag_op Qp.div_2 right_id.
      by apply ufrac_auth_valid. }
    iMod (own_alloc (● ∅)) as (γf) "Hf"; first by apply auth_auth_valid.
    iMod (proph_map_init κs σ.(used_proph_id)) as (?) "Hproph".
    iModIntro. pose (HeapGS _ _ _ γ _ _ γf _ _).
    iExists heap_perm,
      (λ σ κs n, heap_ctx (omap id σ.(heap)) n ∗ proph_map_interp κs σ.(used_proph_id))%I,
      (λ _, heap_fork_post), _, _, True%I.
    iFrame "Hp Hproph". iSplitL "Hσ Hf".
    { iExists ∅. rewrite Hempty to_heapUR_empty fmap_empty big_opM_empty.
      by iFrame. }
    iPoseProof (Hwp _) as "Hwp"; clear Hwp.
    iApply (iron_wp_wand with "[Hwp]").
    { iApply "Hwp". by iEval (rewrite Hempty big_sepM_empty fracPred_at_emp). }
    iIntros (v π1) "Hupd". iExists π1, ε; repeat (iSplit || iModIntro)=> //.
    iIntros (π2 π3 σ2 Qs Hπ) "[Hctx _] _ _".
    iMod ("Hupd" with "Hp'") as (π4 π5 _) "[_ H]".
    iModIntro. iApply ("H" with "Hctx").
  - intros Hwp; eapply (iron_wp_adequacy _ _ _ _ _ _ (1 / 2 / 2) (Some (1 / 2)%Qp)).
    iIntros (? κs) "".
    iMod (own_alloc (●U_1 (to_heapUR (omap id σ.(heap))) ⋅
      (◯U_(1/2) (to_heapUR (omap id σ.(heap))) ⋅ ◯U_(1/2) ε))) as (γ) "[Hσ [Hσ' [Hp Hp']]]".
    { rewrite -ufrac_auth_frag_op Qp.div_2 right_id.
      apply ufrac_auth_valid; by apply to_heapUR_valid. }
    iMod (own_alloc (● ∅)) as (γf) "Hf"; first by apply auth_auth_valid.
    iMod (proph_map_init κs σ.(used_proph_id)) as (?) "Hproph".
    iModIntro. pose (HeapGS _ _ _ γ _ _ γf _ _).
    iExists heap_perm,
      (λ σ κs n, heap_ctx (omap id σ.(heap)) n ∗ proph_map_interp κs σ.(used_proph_id))%I,
      (λ _, heap_fork_post), _, _, True%I.
    iFrame "Hp Hproph". iSplitL "Hσ Hf".
    { iExists ∅. rewrite /= fmap_empty big_opM_empty. by iFrame. }
    iPoseProof (Hwp _) as "Hwp"; clear Hwp.
    iDestruct ("Hwp" with "[Hσ']") as "Hwp'"; first by iApply big_pointsto_alt_2.
    iApply (iron_wp_wand with "Hwp'").
    iIntros (v π1) "Hupd". iExists π1, ε; repeat (iSplit || iModIntro)=> //.
    iIntros (π2 π3 σ2 Qs Hπ) "[Hctx _] _ _".
    iMod ("Hupd" with "Hp'") as (π4 π5 _) "[_ H]".
    iModIntro. iApply ("H" with "Hctx").
Qed.

(** The basic adequacy statement: the program is safe & when the main thread
terminates, the postcondition hold. *)
Theorem heap_basic_adequacy Σ `{heapGpreS Σ} s e σ φ :
  (∀ `{!heapGS Σ},
    {{{ [∗ map] l ↦ v ∈ omap id σ.(heap), l ↦ v }}}
      e @ s; ⊤
    {{{ v, RET v; ⌜φ v⌝ }}}) →
  adequate s e σ (λ v _, φ v).
Proof.
  intros Hwp. apply (heap_adequacy Σ). iIntros (?) "Hall".
  iApply (Hwp with "Hall"). iIntros "!>" (v) "Hφ".
  iMod (fupd_mask_subseteq ∅) as "H"; first done.
  iModIntro. iDestruct "Hφ" as %Hφ. by iIntros "!>" (σ' Qs) "_".
Qed.

(** Adequacy for correct usage of resources: when all threads terminate, and the
post-condition exactly characterizes the heap, we know that the heap is in
fact of the given shape. *)
Theorem heap_all_adequacy Σ `{heapGpreS Σ} s e σ1 σ2 h2 v vs :
  (∀ `{!heapGS Σ},
    ([∗ map] l ↦ v ∈ omap id σ1.(heap), l ↦ v) -∗
    WP e @ s; ⊤ {{ v, ([∗ map] l ↦ v ∈ h2, l ↦ v) }}) →
  rtc erased_step ([e], σ1) (of_val <$> v :: vs, σ2) →
  omap id σ2.(heap) = h2.
Proof.
  (* TODO: refactor this proof so we don't have the two cases *)
  destruct (decide (omap id σ1.(heap) = ∅)) as [Hempty|Heq].
  - intros Hwp Hsteps.
    eapply (iron_wp_all_adequacy _ heap_lang _ _ ∅ σ2 _ _
      (λ _, omap id σ2.(heap) = h2) _ ε); [|done]; iIntros (? κs) "".
    iMod (own_alloc (● ∅)) as (γf) "Hf"; first by apply auth_auth_valid.
    iMod (own_alloc (●U_1 (to_heapUR ∅) ⋅ (◯U_(1/2) (to_heapUR ∅) ⋅ ◯U_(1/2) ε)))
      as (γ) "[Hσ [Hσ' Hp]]".
    { rewrite -ufrac_auth_frag_op Qp.div_2 right_id.
      apply ufrac_auth_valid; by apply to_heapUR_valid. }
    iMod (proph_map_init κs σ1.(used_proph_id)) as (?) "Hproph".
    iModIntro. pose (HeapGS _ _ _ γ _ _ γf _ _).
    iExists heap_perm,
      (λ σ κs n, heap_ctx (omap id σ.(heap)) n ∗ proph_map_interp κs σ.(used_proph_id))%I,
      (λ _, heap_fork_post), _, _.
    iFrame "Hp Hproph". iExists ([∗ map] l ↦ v ∈ h2, l ↦ v)%I. iSplitL "Hσ Hf".
    { iExists ∅. rewrite Hempty to_heapUR_empty fmap_empty big_opM_empty.
      by iFrame. }
    iPoseProof (Hwp _) as "Hwp"; clear Hwp.
    iDestruct ("Hwp" with "[]") as "Hwp'".
    { rewrite Hempty big_opM_empty fracPred_at_emp; auto. }
    iApply (iron_wp_wand with "Hwp'"); iIntros (v' π) "Hσ2 /=".
    iExists π, ε; iSplit; first (by rewrite left_id_L right_id_L).
    iFrame "Hσ2". iModIntro; iSplit; first done.
    iIntros (π1 π2 Hπ) "[Hσ2' _] HQs Hp' Hσ2". rewrite to_heapUR_empty.
    iMod (heap_thread_adequacy _ _ _ (1/2 + π1)%Qp
      with "Hσ2' [HQs] [Hσ' Hp'] Hσ2") as "$".
    { by rewrite -frac_op cmra_op_opM_assoc_L -Hπ frac_op Qp.div_2. }
    { by iApply big_sepL_replicate. }
    { by iSplitL "Hσ'". }
    iMod (fupd_mask_subseteq ∅) as "_"; auto.
  - intros Hwp Hsteps.
    eapply (iron_wp_all_adequacy _ heap_lang _ _ σ1 σ2 _ _
      (λ _, omap id σ2.(heap) = h2) _ (Some (1 / 2)%Qp)); [|done]; iIntros (? κs) "".
    iMod (own_alloc (● ∅)) as (γf) "Hf"; first by apply auth_auth_valid.
    iMod (own_alloc (●U_1 (to_heapUR (omap id σ1.(heap))) ⋅
        (◯U_(1/2) (to_heapUR (omap id σ1.(heap))) ⋅ ◯U_(1/2) ε)))
      as (γ) "[Hσ [Hσ' Hp]]".
    { rewrite -ufrac_auth_frag_op Qp.div_2 right_id.
      apply ufrac_auth_valid; by apply to_heapUR_valid. }
    iMod (proph_map_init κs σ1.(used_proph_id)) as (?) "Hproph".
    iModIntro. pose (HeapGS _ _ _ γ _ _ γf _ _).
    iExists heap_perm,
      (λ σ κs n, heap_ctx (omap id σ.(heap)) n ∗ proph_map_interp κs σ.(used_proph_id))%I,
      (λ _, heap_fork_post), _, _.
    iFrame "Hp Hproph"; iExists ([∗ map] l ↦ v ∈ h2, l ↦ v)%I. iSplitL "Hσ Hf".
    { iExists ∅. rewrite /= fmap_empty big_opM_empty. by iFrame. }
    iPoseProof (Hwp _) as "Hwp"; clear Hwp.
    iDestruct ("Hwp" with "[Hσ']") as "Hwp'"; first by iApply big_pointsto_alt_2.
    iApply (iron_wp_wand with "Hwp'"). iIntros (v' π) "Hσ2".
    iExists π, ε; iSplit; first (erewrite left_id_L, right_id_L; auto; apply _).
    iFrame "Hσ2". iModIntro; iSplit; first done.
    iIntros (π1 π2 Hπ) "[Hσ2' _] HQs Hp' Hσ2".
    iMod (heap_thread_adequacy with "Hσ2' [HQs] Hp' Hσ2") as "$".
    { by rewrite -Hπ /= frac_op Qp.div_2. }
    { by iApply big_sepL_replicate. }
    iMod (fupd_mask_subseteq ∅) as "_"; auto.
Qed.
