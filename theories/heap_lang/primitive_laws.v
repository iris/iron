(** This file provides the weakest preconditions for the constructs of the
heap_lang language. For pure constructors, it makes use of Iris's [PureExec]
infrastructure. For heap-manipulating constructors, there are always two
versions of the rules: The one prefixed with [iron_] for the lifted Iron++ logic,
and the one without that prefix for the basic Iron logic with explicit
fractions (e.g. [iron_wp_alloc] and [wp_alloc]). *)
From iron.iron_logic Require Export weakestpre.
From iris.program_logic Require Import ectx_lifting total_ectx_lifting.
From iris.heap_lang Require Import notation tactics class_instances.
From iron.iron_logic Require Import lifting.
From iron.heap_lang Require Export heap.
From iris.proofmode Require Import proofmode.
Set Default Proof Using "Type".

Section lifting.
Context `{heapGS Σ}.
Implicit Types P Q : ironProp Σ.
Implicit Types Φ : val → ironProp Σ.
Implicit Types ΦΦ : val → iProp Σ.

Lemma wp_fork s E e π ΦΦ :
  ▷ (from_option perm True π -∗ ΦΦ (LitV LitUnit)) -∗
  ▷ WP e @ s; ⊤ {{ _, from_option perm True π }} -∗
  WP Fork e @ s; E {{ ΦΦ }}.
Proof.
  iIntros "HΦ Hwp". iApply wp_lift_base_step_fupd; [done|].
  iIntros (σ1 ns κ κs nt) "[Hh Hκs]".
  iMod (heap_thread_alloc _ _ π with "Hh") as (i) "(Hh & Hp & Hi)".
  iMod (fupd_mask_subseteq∅) as "Hclose"; [set_solver|].
  iModIntro; iSplit; [by auto with base_step|].
  iIntros (e2 σ2 efs Hstep) "_"; inv_base_step.
  iIntros "!> !>". iMod "Hclose" as "_"; iModIntro. iFrame "Hh Hκs".
  iSplitL "Hp HΦ"; first (iApply wp_value'; by iApply "HΦ"). iSplit=> //.
  iApply (wp_wand with "Hwp"); iIntros (v) "Hp". iExists i, π. iFrame.
Qed.
Lemma iron_wp_fork s E e Φ :
  ▷ Φ (LitV LitUnit) -∗ ▷ WP e @ s; ⊤ {{ _, emp }} -∗ WP Fork e @ s; E {{ Φ }}.
Proof.
  iStartProof (iProp _); iIntros (π1) "HΦ"; iIntros (π2) "Hwp".
  rewrite iron_wp_eq; iIntros (π3) "Hp". simpl.
  iApply (wp_fork _ _ _ (Some (π3 ⋅? π2)) with "[HΦ]").
  { iIntros "!> Hp". iExists (π3 ⋅? π2), π1. iFrame.
    by rewrite comm_L cmra_opM_opM_assoc_L. }
  iNext. iApply (wp_wand with "[Hwp Hp]"); [iApply ("Hwp" with "Hp")|].
  iIntros (_). iDestruct 1 as (π1' π2' Hπ) "H".
  rewrite fracPred_at_emp; iDestruct "H" as "[? ->]". by simplify_eq/=.
Qed.

Lemma heap_array_to_seq_pointsto l v (n : nat) :
  ([∗ map] l' ↦ v' ∈ omap id (heap_array l (replicate n v)), l' ↦ v') -∗
  [∗ list] i ∈ seq 0 n, (l +ₗ (i : nat)) ↦ v.
Proof.
  iIntros "Hvs". iInduction n as [|n] "IH" forall (l); simpl.
  { by rewrite omap_empty. }
  rewrite -insert_union_singleton_l omap_insert /=.
  rewrite big_opM_insert; last first.
  { rewrite lookup_omap. apply bind_None; left.
    apply eq_None_not_Some; intros [ow (j&w&?&Hjl&_)%heap_array_lookup].
    rewrite Loc.add_assoc -{1}[l]Loc.add_0 in Hjl. simplify_eq; lia. }
  rewrite Loc.add_0 -fmap_S_seq big_sepL_fmap.
  setoid_rewrite Nat2Z.inj_succ. setoid_rewrite <-Z.add_1_l.
  setoid_rewrite <-Loc.add_assoc.
  iDestruct "Hvs" as "[$ Hvs]". by iApply "IH".
Qed.

Lemma wp_allocN_seq s E v n π :
  (0 < n)%Z →
  {{{ perm π }}} AllocN (Val $ LitV $ LitInt $ n) (Val v) @ s; E
  {{{ l, RET LitV (LitLoc l);
    ([∗ list] i ∈ seq 0 (Z.to_nat n), (l +ₗ (i : nat)) ↦ v) (Some π) }}}.
Proof.
  iIntros (Hn Φ) "Hp HΦ". iApply wp_lift_atomic_base_step_no_fork; first done.
  iIntros (σ1 ns κ κs nt) "[Hσ Hκs] !>"; iSplit;
    first by destruct n; auto with base_step lia.
  iIntros (v2 σ2 efs Hstep) "!> _"; inv_base_step.
  assert (heap_array l (replicate (Z.to_nat n) v) ##ₘ heap σ1).
  { apply heap_array_map_disjoint.
    rewrite length_replicate Z2Nat.id; auto with lia. }
  iMod (heap_alloc_big _ (omap id (heap_array l (replicate (Z.to_nat n) v)))
    with "Hσ Hp") as "(Hσ & Hl)".
  { by apply map_disjoint_omap. }
  { destruct (Z.to_nat n) as [|n'] eqn:?; simpl; [lia|].
    rewrite -insert_union_singleton_l omap_insert /=. apply insert_non_empty. }
  iModIntro. iSplit; first done.
  rewrite map_omap_union //. iFrame "Hκs Hσ". iApply "HΦ".
  by iApply heap_array_to_seq_pointsto.
Qed.
Lemma iron_wp_allocN_seq s E v n :
  (0 < n)%Z →
  {{{ emp }}} AllocN (Val $ LitV $ LitInt $ n) (Val v) @ s; E
  {{{ l, RET LitV (LitLoc l);
    [∗ list] i ∈ seq 0 (Z.to_nat n), (l +ₗ (i : nat)) ↦ v }}}.
Proof.
  iStartProof (iProp _); iIntros (? Φ ? -> π1) "HΦ". rewrite left_id_L.
  iApply iron_wp_lift_alloc. iIntros (π2) "Hp".
  iApply (wp_allocN_seq with "Hp"); first done.
  iIntros "!>" (l) "Hl". iApply ("HΦ" with "Hl").
Qed.

Lemma wp_alloc s E v π :
  {{{ perm π }}} Alloc (Val v) @ s; E {{{ l, RET LitV (LitLoc l); (l ↦ v) (Some π) }}}.
Proof.
  iIntros (Φ) "Hp HΦ". iApply (wp_allocN_seq with "Hp"); simpl; first done.
  iIntros "!>" (l) "Hl". rewrite Loc.add_0 right_id. by iApply "HΦ".
Qed.
Lemma iron_wp_alloc s E v :
  {{{ emp }}} Alloc (Val v) @ s; E {{{ l, RET LitV (LitLoc l); l ↦ v }}}.
Proof.
  iStartProof (iProp _); iIntros (Φ ? -> π1) "HΦ". rewrite left_id_L.
  iApply iron_wp_lift_alloc. iIntros (π2) "Hp". iApply (wp_alloc with "Hp").
  iIntros "!>" (l) "Hl". iApply ("HΦ" with "Hl").
Qed.

Lemma wp_free s E l v π :
  {{{ ▷ (l ↦ v) (Some π) }}} Free (Val (LitV (LitLoc l))) @ s; E
  {{{ RET LitV LitUnit; perm π }}}.
Proof.
  iIntros (Φ) ">Hl HΦ". iApply wp_lift_atomic_base_step_no_fork; auto.
  iIntros (σ ns κ κs nt) "[Hh Hκs] !>".
  iDestruct (heap_valid with "Hh Hl") as %?%lookup_omap_id_Some.
  iSplit; first by eauto with base_step.
  iIntros "!>" (v2 h2 efs Hstep) "_"; inv_base_step.
  iMod (heap_dealloc with "Hh Hl") as "[Hp Hh]".
  rewrite omap_insert /=.
  iModIntro; iSplit; first done. iFrame. by iApply "HΦ".
Qed.
Lemma iron_wp_free s E l v :
  {{{ ▷ (l ↦ v) }}} Free (Val (LitV (LitLoc l))) @ s; E {{{ RET LitV LitUnit; emp }}}.
Proof.
  iStartProof (iProp _); iIntros (Φ [π1|]) "Hl"; iIntros (π2) "HΦ"; last first.
  { iApply iron_wp_lift_free. iDestruct (exist_perm with "Hl") as "> []". }
  iApply iron_wp_lift_free. iApply (wp_free with "Hl"). iIntros "!> Hp".
  iExists π1, π2. iSpecialize ("HΦ" with "[//]"). rewrite right_id_L. by iFrame.
Qed.

Lemma wp_load s E l q v π :
  {{{ ▷ (l ↦{q} v) π }}} Load (Val (LitV (LitLoc l))) @ s; E {{{ RET v; (l ↦{q} v) π }}}.
Proof.
  iIntros (Φ) ">Hl HΦ". iApply wp_lift_atomic_base_step_no_fork; auto.
  iIntros (σ ns κ κs nt) "[Hh Hκs] !>".
  iDestruct (heap_valid with "Hh Hl") as %?%lookup_omap_id_Some.
  iSplit; first by eauto with base_step.
  iIntros "!>" (v2 h2 efs Hstep) "_"; inv_base_step.
  iModIntro; iSplit; first done. iFrame. by iApply "HΦ".
Qed.
Lemma iron_wp_load s E l q v :
  {{{ ▷ l ↦{q} v }}} Load (Val (LitV (LitLoc l))) @ s; E {{{ RET v; l ↦{q} v }}}.
Proof.
  iStartProof (iProp _); iIntros (Φ π) "Hl"; iIntros (π2) "HΦ". rewrite comm_L.
  iApply iron_wp_lift. iApply (wp_load with "Hl"). iIntros "!> Hl". by iApply "HΦ".
Qed.

Lemma wp_store s E l v' v π :
  {{{ ▷ (l ↦ v') π }}} Store (Val (LitV (LitLoc l))) (Val v) @ s; E
  {{{ RET LitV LitUnit; (l ↦ v) π }}}.
Proof.
  iIntros (Φ) ">Hl HΦ". iApply wp_lift_atomic_base_step_no_fork; auto.
  iIntros (σ ns κ κs nt) "[Hh Hκs] !>".
  iDestruct (heap_valid with "Hh Hl") as %?%lookup_omap_id_Some.
  iSplit; first by eauto with base_step.
  iIntros "!>" (v2 h2 efs Hstep) "_"; inv_base_step.
  rewrite omap_insert /=.
  iMod (heap_update with "Hh Hl") as "[$ Hl]".
  iModIntro. iSplit; first done. iFrame. by iApply "HΦ".
Qed.
Lemma iron_wp_store s E l v' v :
  {{{ ▷ l ↦ v' }}} Store (Val (LitV (LitLoc l))) (Val v) @ s; E
  {{{ RET LitV LitUnit; l ↦ v }}}.
Proof.
  iStartProof (iProp _); iIntros (Φ π) "Hl"; iIntros (π2) "HΦ". rewrite comm_L.
  iApply iron_wp_lift. iApply (wp_store with "Hl"). iIntros "!> Hl". by iApply "HΦ".
Qed.

Lemma wp_cmpxchg_fail s E l dq v' v1 v2 π :
  v' ≠ v1 → vals_compare_safe v' v1 →
  {{{ ▷ (l ↦{dq} v') π }}}
    CmpXchg (Val $ LitV $ LitLoc l) (Val v1) (Val v2) @ s; E
  {{{ RET PairV v' (LitV $ LitBool false); (l ↦{dq} v') π }}}.
Proof.
  iIntros (?? Φ) ">Hl HΦ". iApply wp_lift_atomic_base_step_no_fork; first done.
  iIntros (σ1 ns κ κs nt) "[Hh Hκs] !>".
  iDestruct (heap_valid with "Hh Hl") as %?%lookup_omap_id_Some.
  iSplit; first by eauto with base_step.
  iIntros "!>" (v2' σ2 efs Hstep) "_"; inv_base_step.
  rewrite bool_decide_false //.
  iModIntro; iSplit; first done. iFrame. by iApply "HΦ".
Qed.
Lemma iron_wp_cmpxchg_fail s E l dq v' v1 v2 :
  v' ≠ v1 → vals_compare_safe v' v1 →
  {{{ ▷ l ↦{dq} v' }}} CmpXchg (Val $ LitV $ LitLoc l) (Val v1) (Val v2) @ s; E
  {{{ RET PairV v' (LitV $ LitBool false); l ↦{dq} v' }}}.
Proof.
  iStartProof (iProp _); iIntros (?? Φ π) "Hl"; iIntros (π2) "HΦ". rewrite comm_L.
  iApply iron_wp_lift. iApply (wp_cmpxchg_fail with "Hl"); [done..|].
  iIntros "!> Hl". by iApply "HΦ".
Qed.

Lemma wp_cmpxchg_suc s E l v1 v2 v' π :
  v' = v1 → vals_compare_safe v' v1 →
  {{{ ▷ (l ↦ v') π }}} CmpXchg (Val $ LitV $ LitLoc l) (Val v1) (Val v2) @ s; E
  {{{ RET PairV v' (LitV $ LitBool true); (l ↦ v2) π }}}.
Proof.
  iIntros (?? Φ) ">Hl HΦ". iApply wp_lift_atomic_base_step_no_fork; first done.
  iIntros (σ1 ns κ κs nt) "[Hh Hκs] !>".
  iDestruct (heap_valid with "Hh Hl") as %?%lookup_omap_id_Some.
  iSplit; first by eauto with base_step.
  iIntros "!>" (v2' σ2 efs Hstep) "_"; inv_base_step.
  rewrite bool_decide_true //= omap_insert /=.
  iMod (heap_update with "Hh Hl") as "[$ Hl]".
  iModIntro. iSplit; first done. iFrame. by iApply "HΦ".
Qed.
Lemma iron_wp_cmpxchg_suc s E l v1 v2 v' :
  v' = v1 → vals_compare_safe v' v1 →
  {{{ ▷ l ↦ v' }}} CmpXchg (Val $ LitV $ LitLoc l) (Val v1) (Val v2) @ s; E
  {{{ RET PairV v' (LitV $ LitBool true); l ↦ v2 }}}.
Proof.
  iStartProof (iProp _); iIntros (?? Φ π) "Hl"; iIntros (π2) "HΦ". rewrite comm_L.
  iApply iron_wp_lift. iApply (wp_cmpxchg_suc with "Hl"); [done..|].
  iIntros "!> Hl". by iApply "HΦ".
Qed.

Lemma wp_faa s E l i1 i2 π :
  {{{ ▷ (l ↦ LitV (LitInt i1)) π }}}
    FAA (Val (LitV (LitLoc l))) (Val (LitV (LitInt i2))) @ s; E
  {{{ RET LitV (LitInt i1); (l ↦ LitV (LitInt (i1 + i2))) π }}}.
Proof.
  iIntros (Φ) ">Hl HΦ".
  iApply wp_lift_atomic_base_step_no_fork; auto.
  iIntros (σ ns κ κs nt) "[Hh Hκs] !>".
  iDestruct (heap_valid with "Hh Hl") as %?%lookup_omap_id_Some.
  iSplit; first by eauto with base_step.
  iIntros "!>" (v2' h2 efs Hstep) "_"; inv_base_step.
  rewrite omap_insert /=.
  iMod (heap_update with "Hh Hl") as "[$ Hl]".
  iModIntro. iSplit; first done. iFrame. by iApply "HΦ".
Qed.
Lemma iron_wp_faa s E l i1 i2 :
  {{{ ▷ l ↦ LitV (LitInt i1) }}}
    FAA (Val (LitV (LitLoc l))) (Val (LitV (LitInt i2))) @ s; E
  {{{ RET LitV (LitInt i1); l ↦ LitV (LitInt (i1 + i2)) }}}.
Proof.
  iStartProof (iProp _); iIntros (Φ π) "Hl"; iIntros (π2) "HΦ".
  rewrite (comm_L _ π).
  iApply iron_wp_lift. iApply (wp_faa with "Hl"). iIntros "!> Hl".
  by iApply "HΦ".
Qed.
End lifting.
