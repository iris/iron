(* list.v: This is a formalization of a linked list and together
 * with operations for insertion, removal, and disposal. The most
 * intricate of these is the dispoal operation which comes in two
 * flavors: one which does not deallocate the resources in the list
 * and one which does.
 *)
From iris.heap_lang Require Export notation.
From iron.heap_lang Require Import proofmode.
Set Default Proof Using "Type".

Definition new_list : val := λ: <>, ref NONE.
Definition cons : val := λ: "a" "l", ref (SOME ("a", "l")).
Definition mkList : val :=
  rec: "mkList" "n" :=
    if: "n" = #0
    then new_list #()
    else let: "tail" := "mkList" ("n" - #1)
         in cons "n" "tail".

Definition remove : val := λ: "l",
  match: ! "l" with
    NONE => (NONE, "l")
  | SOME "p" =>
    let: "h" := Fst "p" in
    let: "t" := Snd "p" in
    Free "l";;
    (SOME "h", "t")
  end.
Definition delete : val := λ: "l", Free "l".
Definition delete_all : val :=
  rec: "go" "f" "l" :=
    let: "p" := remove "l" in
    match: Fst "p" with
      NONE => delete "l"
    | SOME "x" => "f" "x";; let: "t" := Snd "p" in "go" "f" "t"
    end.

Fixpoint is_list `{!heapGS Σ} (Ψ : val → ironProp Σ)
    (v : val) (ws : list val) : ironProp Σ :=
  (∃ l : loc, ⌜v = #l⌝ ∧
     match ws with
     | [] => l ↦ NONEV
     | w :: ws => ∃ t, l ↦ SOMEV (w, t) ∗ Ψ w ∗ is_list Ψ t ws
     end)%I.

Section proofs.
  Context `{!heapGS Σ} (Ψ : val → ironProp Σ).

  Global Instance is_list_uniform `{Ψ_Uniform : ∀ v, Uniform (Ψ v)} vs v :
    Uniform (is_list Ψ v vs).
  Proof. revert v; induction vs; apply _. Qed.

  Definition new_list_spec :
    {{{ emp }}} new_list #() {{{ v, RET v; is_list Ψ v [] }}}.
  Proof.
    iIntros (Φ) "Hemp HΦ". wp_lam.
    wp_alloc l as "Hl".
    iApply "HΦ"; simpl. eauto with iFrame.
  Qed.

  Definition cons_spec l v vs :
    {{{ Ψ v ∗ is_list Ψ l vs }}} cons v l {{{ l', RET l'; is_list Ψ l' (v :: vs) }}}.
  Proof.
    iIntros (Φ) "[HΨ Hlist] HΦ". wp_lam; wp_pures.
    wp_alloc k as "Hk".
    iApply "HΦ". simpl. eauto 10 with iFrame.
  Qed.

  Fixpoint mkList_ref n :=
    match n with
    | O => []
    | S n => #(S n) :: mkList_ref n
    end.

  Definition mkList_spec (n : nat) :
    □ (∀ (m : nat), Ψ (#m)) -∗
    {{{ emp }}} mkList #n {{{ l, RET l; is_list Ψ l (mkList_ref n) }}}.
  Proof.
    iIntros "#HΨ".
    iLöb as "IH" forall (n).
    iIntros "!#". iIntros (Φ) "Hemp HΦ".
    wp_lam.
    destruct n as [|n]; wp_pures.
    - wp_apply new_list_spec; first done.
      iIntros (v) "Hl"; by iApply "HΦ".
    - rewrite Nat2Z.inj_succ Z.sub_1_r Z.pred_succ.
      wp_apply ("IH" $! n); first done.
      iIntros (l) "Hl".
      wp_let.
      rewrite -Nat2Z.inj_succ.
      wp_apply (cons_spec l #(S n) with "[Hl]").
      { iFrame. by iApply "HΨ". }
      iIntros (l') "Hl'"; by iApply "HΦ".
  Qed.

  Definition remove_spec_nil l :
    {{{ is_list Ψ l [] }}}
      remove l
    {{{ v, RET v; ⌜v = (NONEV, l)%V⌝ ∧ is_list Ψ l [] }}}.
  Proof.
    iIntros (Φ) "Hlist HΦ /=". wp_lam.
    iDestruct "Hlist" as (k ->) "Hk".
    wp_load. wp_pures.
    iApply "HΦ". eauto with iFrame.
  Qed.

  Definition remove_spec_cons l v vs :
    {{{ is_list Ψ l (v :: vs) }}}
      remove l
    {{{ l', RET (SOMEV v, l'); Ψ v ∗ is_list Ψ l' vs }}}.
  Proof.
    iIntros (Φ) "Hlist HΦ /=". wp_lam.
    iDestruct "Hlist" as (k -> t) "(Hk & HΨ & Hlist)".
    wp_load. wp_free. wp_pures.
    iApply "HΦ". iFrame; auto.
  Qed.

  Definition delete_spec l :
    {{{ is_list Ψ l [] }}}
      delete l
    {{{ v, RET v; emp }}}.
  Proof.
    iIntros (Φ) "Hlist HΦ". wp_lam.
    iDestruct "Hlist" as (lk ->) "Hk". wp_free. by iApply "HΦ".
  Qed.

  Definition delete_all_spec (l f : val) vs :
    (∀ v, {{{ Ψ v }}} f v {{{ v, RET v; emp }}}) -∗
    {{{ is_list Ψ l vs }}} delete_all f l {{{ v, RET v; emp }}}.
  Proof.
    iIntros "#Hf !#" (Φ) "Hlist HΦ".
    iInduction vs as [|vs] "IH" forall (l); wp_rec; wp_pures.
    - wp_apply (remove_spec_nil with "Hlist"); iIntros (v') "[-> Hlist]".
      by wp_apply (delete_spec with "Hlist").
    - wp_apply (remove_spec_cons with "Hlist"); iIntros (l') "(HΨ & Hlist)".
      wp_apply ("Hf" with "HΨ"); iIntros (v') "_".
      wp_apply ("IH" with "Hlist HΦ").
  Qed.
End proofs.
