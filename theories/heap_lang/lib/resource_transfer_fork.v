(* resouce_transfer_fork.v: This file contains the verification of one of the resource 
 * transfer examples given in the paper. There are two proofs given: [transfer_works1]
 * follows the proof given in the paper and uses a state transition system.
 * [transfer_works2] is shorter but slightly more intricate.
 *)
From iris.heap_lang Require Export notation.
From iron.heap_lang Require Import proofmode lib.par.
From iron.heap_lang.lib Require Import resource_transfer_sts.
From iron.iron_logic Require Import fcinv.
Set Default Proof Using "Type*".

Definition transfer_alloc_left : val := λ: "m",
  let: "x" := ref #0 in
  "m" <- SOME "x".
Definition transfer_alloc_right : val :=
  rec: "go" "m" :=
    match: !"m" with
      SOME "x" => Free "x";; Free "m"
    | NONE => "go" "m"
    end.

Definition prog : val := λ: "_",
  let: "m" := ref NONE in
  Fork (transfer_alloc_right "m");; transfer_alloc_left "m".

(** The proof that is in the paper using an explicit STS *)
Section proof1.
  Context `{!heapGS Σ, !spawnG Σ, !transferG Σ}.

  Let N := nroot.
  Definition transfer_inv1 (γ : transfer_name) (γinv : fcinv_name) (l : loc) : ironProp Σ :=
    (<affine> ⎡ s₁ γ ⎤ ∗ l ↦ NONEV ∨
     <affine> ⎡ s₂ γ ⎤ ∗ (∃ l' : loc, l ↦ SOMEV (#l') ∗ l' ↦ #0 ∗ fcinv_own γinv (1/2)) ∨ 
     <affine> ⎡ s₃ γ ⎤ ∗ ∃ v, l ↦ v)%I.

  Theorem transfer_works1 :
    {{{ emp }}} prog #() {{{ v, RET v; emp }}}.
  Proof.
    iIntros (Φ) "_ HΦ". wp_lam. wp_alloc l as "Hl"; wp_let.
    iMod transfer_alloc as (γ) "(Ht₁ & Hs₁ & Ht₂)".
    iMod (fcinv_alloc_strong _ N) as (γinv) "[[Hcown₁ Hcown₂] Halloc]".
    iMod ("Halloc" $! (transfer_inv1 γ γinv l) with "[Hs₁ Hl]") as "[#? Hcancel]".
    { iNext. iLeft. iFrame. }
    wp_apply (iron_wp_fork with "[Ht₁ Hcown₁ HΦ]").
    - iNext. wp_pures. wp_lam. wp_alloc k as "Hk". wp_pures.
      iMod (fcinv_acc_strong _ N with "[$] [$]") as "(HInv & [Hcown₁ Hclose])"; first done.
      iDestruct "HInv" as ">[[Hs₁ Hl] | [[Hs₂ H] | [Hs₃ H]]]".
      + wp_store. iDestruct (transfer_combine₁ with "Ht₁ Hs₁") as "Hs₂". 
        iMod ("Hclose" with "[Hcown₁ Hl Hk Hs₂]") as "_".
        { iLeft. iRight. iLeft. eauto with iFrame. }
        iApply ("HΦ" with "[$]").
      + iDestruct (transfer_incompat₁ with "Ht₁ Hs₂") as %[].
      + iDestruct (transfer_incompat₂ with "Ht₁ Hs₃") as %[].
    - iLöb as "IH". wp_rec. wp_bind (! _)%E.
      iMod (fcinv_acc_strong _ N with "[$] [$]") as "(HInv & Hcown₂ & Hclose)"; first done.
      iDestruct "HInv" as ">[[Hs₁ Hl] | [[Hs₂ H] | [Hs₃ H]]]";
        last by iDestruct (transfer_incompat₃ with "Ht₂ Hs₃") as %[].
      + wp_load. iMod ("Hclose" with "[Hl Hs₁]").
        { iLeft. iNext. iLeft. iFrame. }
        iModIntro. wp_match. iApply ("IH" with "[$] [$] [$]").
      + iDestruct "H" as (l') "(Hl & Hl' & Hcown₁)". wp_load.
        iDestruct (transfer_combine₂ with "Ht₂ Hs₂") as "[Ht₃ Hs₃]".
        iMod ("Hclose" with "[Hs₃ Hl]") as "_".
        { iLeft. iModIntro. do 2 iRight. iFrame; eauto. }
        iModIntro. wp_free. wp_seq.
        iMod (fcinv_cancel _ N _ with "[$] Hcancel [$Hcown₁ $Hcown₂]")
          as ">[[Hs₁ Hl] | [[Hs₂ H] | [Hs₃ H]]]"; first done.
        { iDestruct (transfer_incompat₄ with "Ht₃ Hs₁") as %[]. }
        { iDestruct (transfer_incompat₅ with "Ht₃ Hs₂") as %[]. }
        iDestruct "H" as (w) "H". wp_free; auto.
  Qed.
End proof1.

(** A shorter, but more intricate proof, making use of the trackable invariant
tokens. *)
Section proof2.
  Context `{!heapGS Σ, !spawnG Σ}.

  Let N := nroot.
  Definition transfer_inv2 (γ : fcinv_name) (l : loc) : ironProp Σ :=
    (∃ v, l ↦ v ∗
      (<affine> ⌜v = NONEV⌝ ∨
      ∃ l' : loc, ⌜v = SOMEV #l'⌝ ∧ l' ↦ #0 ∗ fcinv_own γ (3/4)))%I.

  Theorem transfer_works2 :
    {{{ emp }}} prog #() {{{ v, RET v; emp }}}.
  Proof.
    iIntros (Φ) "_ HΦ". wp_lam. wp_alloc l as "Hl"; wp_let.
    iMod (fcinv_alloc_strong _ N) as (γ) "[Hγ Halloc]".
    iEval (rewrite -Qp.three_quarter_quarter) in "Hγ"; iDestruct "Hγ" as "[Hγ Hγ']".
    iMod ("Halloc" $! (transfer_inv2 γ l) with "[Hl]") as "[#? Hγc]".
    { iExists NONEV; eauto with iFrame. }
    wp_apply (iron_wp_fork with "[Hγ HΦ]").
    - iIntros "!>". wp_pures. wp_lam. wp_alloc k as "Hk". wp_pures.
      iMod (fcinv_acc_strong _ N with "[$] [$]") as "(Hinv & Hγ & Hclose)"; first done.
      iDestruct "Hinv" as (v) ">[Hl [->|Hinv]]".
      + wp_store. iApply "HΦ". iApply "Hclose".
        iLeft. iExists (SOMEV (#k)). auto 10 with iFrame.
      + iDestruct "Hinv" as (k') "(_&?&Hγ')".
        by iDestruct (fcinv_own_valid with "Hγ Hγ'") as %[].
    - iLöb as "IH"; wp_rec. wp_bind (! _)%E.
      iMod (fcinv_acc_strong _ N with "[$] [$]") as "(Hinv & Hγ & Hclose)"; first done.
      iDestruct "Hinv" as (v) ">[Hl [->|Hinv]]".
      + wp_load. iMod ("Hclose" with "[Hl]").
        { iLeft. iExists NONEV. eauto 10 with iFrame. }
        iModIntro. wp_match. iApply ("IH" with "[$] [$]").
      + wp_load. iDestruct "Hinv" as (k') "(->&Hk'&Hγ1) /=".
        iMod ("Hclose" with "[Hγc Hγ1 Hγ]") as "_".
        { iRight. iEval (rewrite -{2}Qp.three_quarter_quarter). iFrame. }
        iModIntro. wp_match. by do 2 wp_free.
  Qed.
End proof2.
