(* resouce_transfer_par.v: This file contains the verification of one of the resource 
 * transfer examples given in the paper. There are two proofs given: [transfer_works1]
 * follows the proof given in the paper and uses a state transition system.
 * [transfer_works2] is shorter but slightly more intricate.
 *)
From iris.heap_lang Require Export notation.
From iron.heap_lang Require Import proofmode lib.par.
From iron.heap_lang.lib Require Import resource_transfer_sts.
From iron.iron_logic Require Import fcinv.
Set Default Proof Using "Type*".

Definition transfer_alloc_left : val := λ: "m",
  let: "x" := ref #0 in
  "m" <- SOME "x".
Definition transfer_alloc_right : val :=
  rec: "go" "m" :=
    match: !"m" with
      SOME "x" => Free "x"
    | NONE => "go" "m"
    end.

Definition prog : val := λ: "_",
  let: "m" := ref NONE in
  transfer_alloc_left "m" ||| transfer_alloc_right "m";;
  Free "m".

(** The proof that is in the paper using an explicit STS *)
Section proof1.
  Context `{!heapGS Σ, !spawnG Σ, !transferG Σ}.

  Let N := nroot.
  Definition transfer_inv1 (γ : transfer_name) (l : loc) : ironProp Σ :=
    (<affine> ⎡ s₁ γ ⎤ ∗ l ↦ NONEV ∨
     <affine> ⎡ s₂ γ ⎤ ∗ (∃ l' : loc, l ↦ SOMEV (#l') ∗ l' ↦ #0) ∨ 
     <affine> ⎡ s₃ γ ⎤ ∗ ∃ v, l ↦ v)%I.

  Theorem transfer_works1 :
    {{{ emp }}} prog #() {{{ v, RET v; emp }}}.
  Proof.
    iIntros (Φ) "_ HΦ". wp_lam. wp_alloc l as "Hl"; wp_let.
    iMod transfer_alloc as (γ) "(Ht₁ & Hs₁ & Ht₂)".
    iMod (fcinv_alloc _ N (transfer_inv1 γ l) with "[Hs₁ Hl]")
      as (γinv) "(#Hinv & Hcancel & [Hcown₁ Hcown₂])".
    { iNext. iLeft. iFrame. }
    wp_apply (par_spec (λ _, fcinv_own γinv (1 / 2))%I
      (λ _, fcinv_own γinv (1 / 2) ∗ <affine> ⎡ t₃ γ ⎤)%I with "[Ht₁ Hcown₁] [Ht₂ Hcown₂]").
    - do 2 wp_lam. wp_alloc k as "Hk". wp_pures.
      iMod (fcinv_acc _ N with "[$] [$]") as "(HInv & $ & Hclose)"; first done.
      iDestruct "HInv" as ">[[Hs₁ Hl] | [[Hs₂ H] | [Hs₃ H]]]".
      + wp_store. iDestruct (transfer_combine₁ with "Ht₁ Hs₁") as "Hs₂".
        iApply "Hclose". iNext. iRight. iLeft. eauto with iFrame.
      + iDestruct (transfer_incompat₁ with "Ht₁ Hs₂") as %[].
      + iDestruct (transfer_incompat₂ with "Ht₁ Hs₃") as %[].
    - wp_lam. iLöb as "IH"; wp_rec. wp_bind (! _)%E.
      iMod (fcinv_acc _ N with "[$] [$]") as "(HInv & Hγ & Hclose)"; first done.
      iDestruct "HInv" as ">[[Hs₁ Hl] | [[Hs₂ H] | [Hs₃ H]]]";
        last (iDestruct (transfer_incompat₃ with "Ht₂ Hs₃") as %[]).
      + wp_load. iMod ("Hclose" with "[Hl Hs₁]").
        { iNext. iLeft. iFrame. }
        iModIntro. wp_match. iApply ("IH" with "[$] [$]").
      + iDestruct "H" as (l') "[Hl Hl']". wp_load.
        iDestruct (transfer_combine₂ with "Ht₂ Hs₂") as "[Ht₃ Hs₃]".
        iMod ("Hclose" with "[Hs₃ Hl]") as "Hcown₂".
        { iNext. do 2 iRight. iFrame; eauto. }
        iModIntro. wp_free. iFrame.
    - iIntros (v1 v2) "[Hown₁ [Hown₂ Ht₃]] !>". 
      iMod (fcinv_cancel _ N _ with "Hinv Hcancel [$Hown₁ $Hown₂]")
        as ">[[Hs₁ Hl] | [[Hs₂ H] | [Hs₃ H]]]"; first done.
      { iDestruct (transfer_incompat₄ with "Ht₃ Hs₁") as %[]. }
      { iDestruct (transfer_incompat₅ with "Ht₃ Hs₂") as %[]. }
      wp_seq. iDestruct "H" as (w) "H". wp_free. by iApply "HΦ".
  Qed.
End proof1.

(** A shorter, but more intricate proof, making use of the trackable invariant
tokens. *)
Section proof2.
  Context `{!heapGS Σ, !spawnG Σ}.

  Let N := nroot.
  Definition transfer_inv2 (γ : fcinv_name) (l : loc) : ironProp Σ :=
    (∃ v, l ↦ v ∗
      (<affine> ⌜v = NONEV⌝ ∨
      ∃ l' : loc, ⌜v = SOMEV #l'⌝ ∧ l' ↦ #0 ∗ fcinv_own γ (3/4)))%I.

  Theorem transfer_works2 :
    {{{ emp }}} prog #() {{{ v, RET v; emp }}}.
  Proof.
    iIntros (Φ) "_ HΦ". wp_lam. wp_alloc l as "Hl"; wp_let.
    iMod (fcinv_alloc_strong _ N) as (γ) "[Hγ Halloc]".
    iEval (rewrite -Qp.three_quarter_quarter) in "Hγ"; iDestruct "Hγ" as "[Hγ Hγ']".
    iMod ("Halloc" $! (transfer_inv2 γ l) with "[Hl]") as "[#? Hγc]".
    { iExists NONEV; eauto with iFrame. }
    wp_apply (par_spec (λ _, emp)%I (λ _, ∃ v, l ↦ v)%I with "[Hγ] [Hγc Hγ']").
    - do 2 wp_lam. wp_alloc k as "Hk". wp_pures.
      iMod (fcinv_acc_strong _ N with "[$] [$]") as "(Hinv & Hγ & Hclose)"; first done.
      iDestruct "Hinv" as (v) ">[Hl [->|Hinv]]".
      + wp_store. iApply "Hclose". iLeft. iExists (SOMEV (#k)). auto 10 with iFrame.
      + iDestruct "Hinv" as (k') "(_&?&Hγ')".
        by iDestruct (fcinv_own_valid with "Hγ Hγ'") as %[].
    - wp_lam. iLöb as "IH"; wp_rec. wp_bind (! _)%E.
      iMod (fcinv_acc_strong _ N with "[$] [$]") as "(Hinv & Hγ & Hclose)"; first done.
      iDestruct "Hinv" as (v) ">[Hl [->|Hinv]]".
      + wp_load. iMod ("Hclose" with "[Hl]").
        { iLeft. iExists NONEV. eauto 10 with iFrame. }
        iModIntro. wp_match. iApply ("IH" with "[$] [$]").
      + wp_load. iDestruct "Hinv" as (k') "(->&Hk'&Hγ1) /=".
        iMod ("Hclose" with "[Hγc Hγ1 Hγ]") as "_".
        { iRight. iEval (rewrite -{2}Qp.three_quarter_quarter). iFrame. }
        iModIntro. wp_free. auto.
    - iIntros (v1 v2) "[_ Hl] !>". iDestruct "Hl" as (v) "Hl".
      wp_free. by iApply "HΦ".
  Qed.
End proof2.
