(* spawn.v: This file contains the implementation of the spawn primitive. It is
 * the only proof which we use the unlifted logic directly for. At the end we present
 * lifted specifications suitable for use by proofs purely within the lifted logic
 * however.
 *
 * The main proofs are [spawn_spec'] and [join_spec'] and their liftings are in
 * [spawn_spec] and [join_spec].
 *)
From iris.heap_lang Require Export lang notation.
From iron.heap_lang Require Import proofmode.
From iron.heap_lang.lib Require Import resource_transfer_sts.
From iron.iron_logic Require Import fcinv.
Set Default Proof Using "Type".

Definition spawn : val :=
  λ: "f",
    let: "c" := ref NONE in
    Fork ("c" <- SOME ("f" #())) ;; "c".
Definition join : val :=
  rec: "join" "c" :=
    match: !"c" with
      SOME "x" => Free "c";; "x"
    | NONE => "join" "c"
    end.

(** The CMRA & functor we need. *)
(* Not bundling heapGS, as it may be shared with other users. *)
Class spawnG Σ := SpawnG { spawn_transferG : transferG Σ }.
Local Existing Instance spawn_transferG.
Definition spawnΣ : gFunctors := transferΣ.

Global Instance subG_spawnΣ {Σ} : subG spawnΣ Σ → spawnG Σ.
Proof. solve_inG. Qed.

(** First we give the spec in the unlifted logic, like we have done in the
paper. *)
Definition spawn_inv `{!heapGS Σ, !spawnG Σ} (γ : transfer_name) (l : loc)
    (π : frac) (Ψ : val → iProp Σ) : iProp Σ :=
  (s₁ γ ∗ (l ↦ NONEV) (Some π) ∨
   s₂ γ ∗ (∃ w, (l ↦ SOMEV w) (Some π) ∗ Ψ w) ∨
   s₃ γ)%I.

Definition join_handle' `{!heapGS Σ, !spawnG Σ} (N : namespace)
    (l : loc) (π : frac) (Ψ : val → iProp Σ) : iProp Σ :=
  (∃ γ, t₂ γ ∗ inv N (spawn_inv γ l π Ψ))%I.

(** Then the lift the spec to the lifted logic *)
Definition join_handle `{!heapGS Σ, !spawnG Σ} (N : namespace)
    (l : loc) (Ψ : val → ironProp Σ) : ironProp Σ :=
  FracPred (λ π, ∃ π1 π2, ⌜ π = Some (π1 ⋅ π2) ⌝ ∧
                 join_handle' N l π1 (λ v, ∃ π21 π22,
                   ⌜ π2 = π21 ⋅? π22 ⌝ ∧ perm π21 ∗ Ψ v π22))%I.

Section proof.
Context `{!heapGS Σ, !spawnG Σ} (N : namespace).

Global Instance spawn_inv_ne n γ l π :
  Proper (pointwise_relation val (dist n) ==> dist n) (spawn_inv γ l π).
Proof. solve_proper. Qed.
Global Instance join_handle'_ne n l π :
  Proper (pointwise_relation val (dist n) ==> dist n) (join_handle' N l π).
Proof. solve_proper. Qed.
Global Instance join_handle_ne n l :
  Proper (pointwise_relation val (dist n) ==> dist n) (join_handle N l).
Proof. intros Ψ1 Ψ2 HΨ; split; solve_proper. Qed.

Opaque perm.

(** The main proofs in the unlifted logic *)
Lemma spawn_spec' (Ψ : val → iProp Σ) (f : val) π :
  {{{ perm π ∗ WP f #() {{ Ψ }} }}} spawn f {{{ l, RET #l; join_handle' N l π Ψ }}}.
Proof.
  iIntros (Φ) "[Hp Hf] HΦ".
  wp_lam. wp_apply (wp_alloc with "Hp"); iIntros (l) "Hl"; wp_let.
  iMod transfer_alloc as (γ) "(Ht₁ & Hs₁ & Ht₂)".
  iMod (inv_alloc N _ (spawn_inv γ l π Ψ) with "[Hl Hs₁]") as "#?".
  { iNext. iLeft. iFrame. }
  wp_apply (wp_fork _ _ _ None with "[HΦ Ht₂]"); simpl.
  { iIntros "!> _". wp_seq. iApply "HΦ". iExists γ. eauto. }
  wp_apply (wp_wand with "Hf"); iIntros (v) "HΨ".
  wp_pures. iInv N as "[>[Hs₁ Hl] | [[>Hs₂ _] | >Hs₃]]".
  - iDestruct (transfer_combine₁ with "Ht₁ Hs₁") as "Hs₂".
    wp_apply (wp_store with "Hl"); iIntros "Hl !>". iSplitL; last done.
    iNext. iRight; iLeft. eauto with iFrame.
  - iDestruct (transfer_incompat₁ with "Ht₁ Hs₂") as "[]".
  - iDestruct (transfer_incompat₂ with "Ht₁ Hs₃") as "[]".
Qed.

Lemma join_spec' (Ψ : val → iProp Σ) l π :
  {{{ join_handle' N l π Ψ }}} join #l {{{ v, RET v; perm π ∗ Ψ v }}}.
Proof.
  iIntros (Φ) "H HΦ". iDestruct "H" as (γ) "[Ht₂ #?]".
  iLöb as "IH". wp_rec. wp_bind (! _)%E.
  iInv N as "[>[Hs₁ Hl] | [[>Hs₂ Hinv] | >Hs₃]]".
  - wp_apply (wp_load with "Hl"); iIntros "Hl !>". iSplitL "Hs₁ Hl".
    { iNext. iLeft. iFrame. }
    wp_match. iApply ("IH" with "Ht₂ HΦ").
  - iDestruct "Hinv" as (w) "[Hl HΨ]".
    wp_apply (wp_load with "Hl"); iIntros "Hl !>". iSplitL "Ht₂ Hs₂".
    { iNext. do 2 iRight. iDestruct (transfer_combine₂ with "Ht₂ Hs₂") as "[_ $]". }
    wp_match. wp_apply (wp_free with "Hl"); iIntros "/= Hp'"; wp_seq.
    iApply "HΦ"; iFrame.
  - iDestruct (transfer_incompat₃ with "Ht₂ Hs₃") as "[]".
Qed.

(** The main proofs in the lifted logic *)
Lemma spawn_spec (Ψ : val → ironProp Σ) (f : val) :
  {{{ WP f #() {{ Ψ }} }}} spawn f {{{ l, RET #l; join_handle N l Ψ }}}.
Proof.
  iStartProof (iProp _).
  iIntros (Φ π1) "Hf"; iIntros (π2) "HΦ". iApply lifting.iron_wp_lift_alloc.
  iIntros (π3) "[Hp Hp']". rewrite iron_wp_eq /=. iSpecialize ("Hf" with "Hp'").
  iApply (spawn_spec' with "[$Hp $Hf]").
  iIntros "!>" (l) "H". rewrite (comm_L _ π1) -assoc_L. iApply "HΦ".
  iExists (π3 / 2)%Qp, ((π3 / 2)%Qp ⋅? π1). iFrame "H".
  by rewrite -cmra_op_opM_assoc_L frac_op Qp.div_2 -Some_op_opM comm_L.
Qed.

Lemma join_spec (Ψ : val → ironProp Σ) l :
  {{{ join_handle N l Ψ }}} join #l {{{ v, RET v; Ψ v }}}.
Proof.
  iStartProof (iProp _). iIntros (Φ π1) "H"; iIntros (π2) "HΦ".
  iDestruct "H" as (π3 π4 ->) "H". iApply lifting.iron_wp_lift_free.
  wp_apply (join_spec' with "H"). iIntros (v) "[Hp H]".
  iDestruct "H" as (π5 π6 ->) "[Hp' HΨ]".
  iExists (π3 ⋅ π5), (π6 ⋅ π2). iSplit.
  { by rewrite !Some_op -Some_op_opM !assoc_L. }
  iFrame. rewrite comm_L. by iApply "HΦ".
Qed.
End proof.

Global Typeclasses Opaque join_handle.
