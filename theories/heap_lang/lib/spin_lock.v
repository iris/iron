(* spin_lock.v: This file contains a verification of the standard spin-lock implementation
 * supplemented with a free operation. It does not ensure that a lock that is acquired
 * must be released and the implementation of free has to lock the lock before disposal.
 *)
From iris.heap_lang Require Export notation.
From iron.heap_lang Require Import proofmode.
From iron.iron_logic Require Import fcinv.
From iris.algebra Require Import excl.
Set Default Proof Using "Type".

Definition new_lock : val := λ: <>, ref #false.
Definition try_acquire : val := λ: "l", CAS "l" #false #true.
Definition acquire : val :=
  rec: "acquire" "l" :=
    if: try_acquire "l"
    then #()
    else "acquire" "l".
Definition release : val := λ: "l", "l" <- #false.
Definition free : val :=
  rec: "free" "l" := if: !"l" = #false then Free "l" else "free" "l".

(** The CMRA we need. *)
(* Not bundling heapGS, as it may be shared with other users. *)
Class lockG Σ := LockG { lock_tokG :: inG Σ (exclR unitO) }.
Definition lockΣ : gFunctors := #[GFunctor (exclR unitO)].

Global Instance subG_lockΣ {Σ} : subG lockΣ Σ → lockG Σ.
Proof. solve_inG. Qed.

Record lock_name := LockName {
  lock_fcinv_name : fcinv_name;
  lock_excl_name : gname;
}.

Definition lock_inv `{!heapGS Σ, !lockG Σ} (γ : gname)
    (l : loc) (R : ironProp Σ) : ironProp Σ :=
  (∃ b : bool, l ↦ #b ∗
   if b then emp else <affine> ⎡ own γ (Excl ()) ⎤ ∗ R)%I.

Definition is_lock `{!heapGS Σ, !lockG Σ} (N : namespace) (γ : lock_name)
    (lk : val) (p : frac) (R : ironProp Σ) : ironProp Σ :=
  (∃ l : loc, ⌜lk = #l⌝ ∧
              fcinv N (lock_fcinv_name γ) (lock_inv (lock_excl_name γ) l R) ∗
              fcinv_cancel_own (lock_fcinv_name γ) p ∗
              fcinv_own (lock_fcinv_name γ) p)%I.

Definition locked `{!heapGS Σ, !lockG Σ} (γ : lock_name) : ironProp Σ :=
  (<affine> ⎡ own (lock_excl_name γ) (Excl ()) ⎤)%I.

Section proof.
  Context `{!heapGS Σ, !lockG Σ} (N : namespace).

  Global Instance is_lock_fractional γ lk R : Fractional (λ p, is_lock N γ lk p R).
  Proof.
    intros q1 q2. rewrite /is_lock; iSplit.
    - iDestruct 1 as (l ->) "(#? & [H2 H2'] & [H3 H3'])".
      iSplitL "H2 H3"; iExists _; iFrame; eauto.
    - iIntros "[H1 H2]".
      iDestruct "H1" as (l ->) "(#? & H3 & H4)".
      iDestruct "H2" as (l' ->) "(#? & H3' & H4')".
      iExists _; iFrame; eauto.
  Qed.
  Global Instance is_lock_as_fractional γ lk p R :
    AsFractional (is_lock N γ lk p R) (λ p, is_lock N γ lk p R) p.
  Proof. split. done. apply _. Qed.

  Lemma locked_exclusive (γ : lock_name)  :
    locked γ -∗ locked γ -∗ False.
  Proof. iIntros "H1 H2". by iCombine "H1 H2" gives %?. Qed.

  Instance lock_inv_ne γ l : NonExpansive (lock_inv γ l).
  Proof. solve_proper. Qed.
  Instance lock_inv_uniform γ l R : Uniform R → Uniform (lock_inv γ l R).
  Proof. intros. apply exist_uniform=> -[]; apply _. Qed.
  Global Instance is_lock_ne γ l p : Contractive (is_lock N γ l p).
  Proof. solve_contractive. Qed.

  Global Instance locked_timeless γ l : Timeless (locked γ l).
  Proof. apply _. Qed.

  (** The main proofs. *)
  Lemma new_lock_spec R :
    {{{ R }}} new_lock #() {{{ lk γ, RET lk; is_lock N γ lk 1 R }}}.
  Proof.
    iIntros (Φ) "HR HΦ". rewrite -iron_wp_fupd /new_lock /=.
    wp_lam. wp_alloc l as "Hl".
    iMod (own_alloc (Excl ())) as (γ) "Hγ"; first done.
    iMod (fcinv_alloc _ N (lock_inv γ l R) with "[-HΦ]") as (γinv) "(#? & Hγ & Hγc)".
    { iIntros "!>". iExists false. by iFrame. }
    iModIntro. iApply ("HΦ" $! #l (LockName γinv γ)). iExists l; iFrame; eauto.
  Qed.

  Lemma try_acquire_spec p γ lk R `{!Uniform R} :
    {{{ is_lock N γ lk p R }}} try_acquire lk
    {{{ b, RET #b; is_lock N γ lk p R ∗ if b is true then locked γ ∗ R else emp }}}.
  Proof.
    iIntros (Φ) "Hl HΦ". iDestruct "Hl" as (l ->) "(#? & Hγinvc & Hγinv)".
    wp_lam. wp_bind (CmpXchg _ _ _).
    iMod (fcinv_acc _ N with "[$] [$]") as "(Hinv & Hγinv & Hclose)"; first done.
    iDestruct "Hinv" as ([|]) "[>Hl H]".
    - wp_cmpxchg_fail. iMod ("Hclose" with "[Hl]"); first (iNext; iExists true; eauto).
      iModIntro. wp_pures. iApply ("HΦ" $! false). iSplit; last done.
      by iFrame "#∗".
    - wp_cmpxchg_suc. iDestruct "H" as "[Hγ HR]".
      iMod ("Hclose" with "[Hl]"); first (iNext; iExists true; eauto).
      iModIntro. wp_pures. iApply ("HΦ" $! true with "[-]").
      by iFrame "#∗".
  Qed.

  Lemma acquire_spec p γ lk R `{!Uniform R} :
    {{{ is_lock N γ lk p R }}} acquire lk
    {{{ RET #(); is_lock N γ lk p R ∗ locked γ ∗ R }}}.
  Proof.
    iIntros (Φ) "Hl HΦ". iLöb as "IH". wp_rec.
    wp_apply (try_acquire_spec with "Hl"). iIntros ([]).
    - iIntros "[Hlked HR]". wp_if.
      iApply "HΦ"; iFrame.
    - iIntros "[Hlock _]". wp_if. iApply ("IH" with "[Hlock]"); auto.
  Qed.

  Lemma release_spec p γ lk R `{!Uniform R} :
    {{{ is_lock N γ lk p R ∗ locked γ ∗ R }}} release lk
    {{{ RET #(); is_lock N γ lk p R }}}.
  Proof.
    iIntros (Φ) "(Hlock & Hγ & HR) HΦ".
    iDestruct "Hlock" as (l ->) "(#? & Hγinvc & Hγinv)". wp_lam.
    iMod (fcinv_acc _ N with "[$] [$]") as "(Hinv & Hγinv & Hclose)"; first done.
    iDestruct "Hinv" as ([|]) "[Hl Hinv]".
    - wp_store. iMod ("Hclose" with "[HR Hl Hγ]").
      { iNext. iExists false; iFrame. }
      iApply "HΦ". rewrite /is_lock. eauto with iFrame.
    - iDestruct "Hinv" as "[>Hγ' ?]".
      iDestruct (locked_exclusive with "[$] [$]") as "[]".
  Qed.

  Lemma free_spec γ lk R `{!Uniform R} :
    {{{ is_lock N γ lk 1 R }}} free lk {{{ RET #(); R }}}.
  Proof.
    iIntros (Φ) "Hlock HΦ". iDestruct "Hlock" as (l ->) "(#? & Hγinv & Hγinvc)".
    iMod (fcinv_cancel _ N with "[$] [$] [$]") as (b) "[Ht Hinv]"; auto.
    iLöb as "IH" forall (b); wp_rec. destruct b.
    - wp_load. wp_op. wp_if. by iApply ("IH" with "HΦ Ht").
    - wp_load. wp_free. iDestruct "Hinv" as "[_ HR]". by iApply "HΦ".
  Qed.
End proof.

Global Typeclasses Opaque is_lock locked.
