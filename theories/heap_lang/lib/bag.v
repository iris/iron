(* bag.v: this formalizes the bag example not used in the paper.
 * It, in particular, includes operations for creating a new bag, insertion,
 * removal, and disposal of the entire bag. The specifications for these
 * operations is given by [new_bag_spec], [insert_spec], [remove_spec],
 * and [dispose_spec].
 *)
From iris.heap_lang Require Export notation.
From iron.heap_lang Require Import proofmode.
From iron.heap_lang.lib Require Import spin_lock list.
Set Default Proof Using "Type".

Definition new_bag : val := λ: <>,
  let: "list" := ref (new_list #()) in
  let: "lock" := new_lock #() in
  ("list", "lock").

Definition insert : val := λ: "bag" "u",
  let: "lock" := Snd "bag" in
  acquire "lock";;
  let: "list" := ! (Fst "bag") in
  Fst "bag" <- (cons "u" "list");;
  release "lock".

Definition remove : val := λ: "bag",
  let: "lock" := Snd "bag" in
  acquire "lock";;
  let: "list" := !(Fst "bag") in
  let: "p" := remove "list" in
  Fst "bag" <- Snd "p";;
  release "lock";;
  Fst "p".

Definition dispose : val := λ: "bag" "f",
  let: "lock" := Snd "bag" in
  free "lock";;
  let: "list" := !(Fst "bag") in
  Free (Fst "bag");;
  delete_all "f" "list".

Definition bag_inv `{!heapGS Σ} (v : val) (Ψ : val → ironProp Σ) : ironProp Σ :=
  (∃ (k : loc) (l : val) (vs : list val),
    ⌜v = #k⌝ ∧ k ↦ l ∗ is_list Ψ l vs)%I.
Definition is_bag `{!heapGS Σ, !lockG Σ} (N : namespace)
    (γ : lock_name) (v : val) (p : frac) (Ψ : val → ironProp Σ) : ironProp Σ:=
  (∃ v1 v2, ⌜v = (v1, v2)%V⌝ ∧ is_lock N γ v2 p (bag_inv v1 Ψ))%I.

Section proofs.
  Context `{!heapGS Σ, !lockG Σ} (N : namespace).
  Context (Ψ : val → ironProp Σ) `{HΨ : ∀ v : val, Uniform (Ψ v)}.

  Lemma into_bag_inv l v ws : l ↦ v -∗ is_list Ψ v ws -∗ bag_inv #l Ψ.
  Proof. iIntros "H1 H2"; iExists _, _, _; iFrame; eauto. Qed.

  Global Instance is_bag_fractional γ b : Fractional (λ p, is_bag N γ b p Ψ).
  Proof.
    intros p1 p2. iSplit.
    - iDestruct 1 as (k l ->) "[H1 H2]".
      iSplitL "H1"; iExists _, _; iFrame; eauto.
    - iIntros "[H1 H2]".
      iDestruct "H1" as (k1 l1 ->) "H1".
      iDestruct "H2" as (k2 l2 ?) "H2"; simplify_eq/=.
      iExists _, _; iSplit; eauto. by iSplitL "H1".
  Qed.
  Global Instance is_bag_as_fractional γ b p :
    AsFractional (is_bag N γ b p Ψ) (λ p, is_bag N γ b p Ψ) p.
  Proof. split. done. apply _. Qed.

  Local Instance bag_inv_uniform v : Uniform (bag_inv v Ψ).
  Proof using HΨ. rewrite /bag_inv; apply _. Qed.

  Theorem new_bag_spec :
    {{{ emp }}} new_bag #() {{{ b γ, RET b; is_bag N γ b 1 Ψ }}}.
  Proof.
    iIntros (Φ) "_ HΦ". wp_lam.
    wp_apply (new_list_spec Ψ with "[//]"); iIntros (l) "Hlist".
    wp_alloc k as "Hk".
    iDestruct (into_bag_inv with "Hk Hlist") as "Hlist".
    wp_apply (new_lock_spec with "Hlist"); iIntros (lk γ) "Hlock".
    wp_pures. iApply "HΦ". rewrite /is_bag; eauto.
  Qed.

  Theorem insert_spec γ b v p :
    {{{ is_bag N γ b p Ψ ∗ Ψ v }}}
      insert b v
    {{{ RET #(); is_bag N γ b p Ψ }}}.
  Proof using HΨ.
    iIntros (Φ) "[Hbag HΨ] HΦ". wp_lam; wp_pures.
    iDestruct "Hbag" as (v1 v2 ->) "Hlock".
    wp_apply (acquire_spec with "Hlock"); iIntros "(Hlock & Hlocked & Hbag)".
    iDestruct "Hbag" as (k l xs ->) "[Hk Hlist]".
    wp_load.
    wp_apply (cons_spec with "[$Hlist $HΨ]"); iIntros (l') "Hlist".
    wp_store.
    wp_apply (release_spec with "[$Hlock $Hlocked Hk Hlist]").
    { iApply (into_bag_inv with "Hk Hlist"). }
    iIntros "Hlock". iApply "HΦ". rewrite /is_bag; auto.
  Qed.

  Theorem remove_spec γ b p :
    {{{ is_bag N γ b p Ψ }}}
      remove b
    {{{ v, RET v;
         (⌜v = NONEV⌝ ∧ is_bag N γ b p Ψ) ∨
         (∃ v', ⌜v = SOMEV v'⌝ ∧ Ψ v' ∗ is_bag N γ b p Ψ) }}}.
  Proof using HΨ.
    iIntros (Φ) "Hbag HΦ". wp_lam.
    iDestruct "Hbag" as (v₁ v₂ ->) "Hlock".
    wp_apply (acquire_spec with "Hlock"); iIntros "(Hlock & Hlocked & Hbag)".
    iDestruct "Hbag" as (k l xs ->) "[Hk Hlist]".
    wp_load.
    destruct xs as [|xs].
    - wp_apply (remove_spec_nil with "Hlist"); iIntros (l') "[-> Hlist]".
      wp_store.
      wp_apply (release_spec with "[$Hlock $Hlocked Hk Hlist]").
      { iApply (into_bag_inv with "Hk Hlist"). }
      iIntros "Hlock".
      wp_pures. iApply "HΦ". rewrite /is_bag; eauto 10.
    - wp_apply (remove_spec_cons with "Hlist"); iIntros (l') "(HΨ & Hlist)".
      wp_store.
      wp_apply (release_spec with "[$Hlock $Hlocked Hk Hlist]").
      { iApply (into_bag_inv with "Hk Hlist"). }
      iIntros "Hlock".
      wp_pures. iApply "HΦ". rewrite /is_bag; auto 10 with iFrame.
  Qed.

  Theorem dispose_spec γ (f b : val) :
    (∀ v, {{{ Ψ v }}} f v {{{ v, RET v; emp }}}) -∗
    {{{ is_bag N γ b 1 Ψ }}}
      dispose b f
    {{{ v, RET v; emp }}}.
  Proof using HΨ.
    iIntros "#Hf !#" (Φ) "Hbag HΦ". wp_lam.
    iDestruct "Hbag" as (v₁ v₂ ->) "Hlock".
    wp_apply (free_spec with "Hlock"). iIntros "Hbag".
    iDestruct "Hbag" as (ℓ l xs) "[-> [Hℓ Hlist]]".
    wp_load. wp_free. wp_apply (delete_all_spec Ψ with "[] [Hlist]"); eauto.
  Qed.
End proofs.
