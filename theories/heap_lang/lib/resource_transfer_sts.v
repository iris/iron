(* resource_transfer_sts.v: All of the examples in the paper for resource transfer make
 * use of a transistion system encoded as a camera. This has been extracted out into this
 * file so that it can be reused. The rules for the state transition system (what updates
 * are allowed, what states are illegal) are proven in this file.
 *)
From iron.iron_logic Require Export iron.
From iris.proofmode Require Import proofmode.
From iris.algebra Require Import excl.

Record transfer_name := { transfer_name₁ : gname; transfer_name₂ : gname; }.

Class transferG Σ := TransferG {
  transfer_tokG : inG Σ (exclR unitO);
  transfer_fracG : inG Σ fracR
}.
Local Existing Instances transfer_tokG transfer_fracG.
Definition transferΣ : gFunctors :=
  #[GFunctor (exclR unitO); GFunctor fracR].

Global Instance subG_transferΣ {Σ} : subG transferΣ Σ → transferG Σ.
Proof. solve_inG. Qed.

Definition s₁ `{transferG Σ} (γ : transfer_name) : iProp Σ :=
  (own (transfer_name₁ γ) (1/2/2)%Qp ∗ own (transfer_name₂ γ) (Excl ()))%I.
Definition t₁ `{transferG Σ} (γ : transfer_name) : iProp Σ :=
  own (transfer_name₁ γ) (1/2)%Qp.
Definition s₂ `{transferG Σ} (γ : transfer_name) : iProp Σ :=
  (own (transfer_name₁ γ) (1/2 ⋅ 1/2/2)%Qp ∗ own (transfer_name₂ γ) (Excl ()))%I.
Definition t₂ `{transferG Σ} (γ : transfer_name) : iProp Σ :=
  own (transfer_name₁ γ) (1/2/2)%Qp.
Definition s₃ `{transferG Σ} (γ : transfer_name) : iProp Σ :=
  own (transfer_name₁ γ) 1%Qp.
Definition t₃ `{transferG Σ} (γ : transfer_name) : iProp Σ :=
  own (transfer_name₂ γ) (Excl ()).

Section sts.
  Context `{!transferG Σ}.
  Implicit Types γ : transfer_name.

  Lemma transfer_incompat₁ γ : t₁ γ -∗ s₂ γ -∗ False.
  Proof.
    iIntros "H1 [H2 _]".
    by iCombine "H1 H2" gives %?.
  Qed.
  Lemma transfer_incompat₂ γ : t₁ γ -∗ s₃ γ -∗ False.
  Proof.
    iIntros "H1 H2".
    by iCombine "H1 H2" gives %?.
  Qed.
  Lemma transfer_incompat₃ γ : t₂ γ -∗ s₃ γ -∗ False.
  Proof.
    iIntros "H1 H2".
    by iCombine "H1 H2" gives %?.
  Qed.
  Lemma transfer_incompat₄ γ : t₃ γ -∗ s₁ γ -∗ False.
  Proof.
    iIntros "H1 [_ H2]".
    by iCombine "H1 H2" gives %?.
  Qed.
  Lemma transfer_incompat₅ γ : t₃ γ -∗ s₂ γ -∗ False.
  Proof.
    iIntros "H1 [_ H2]".
    by iCombine "H1 H2" gives %?.
  Qed.
  Lemma transfer_combine₁ γ : t₁ γ -∗ s₁ γ -∗ s₂ γ.
  Proof.
    iIntros "H2 [H1 $]". by iCombine "H2" "H1" as "HH".
  Qed.
  Lemma transfer_combine₂ γ : t₂ γ -∗ s₂ γ -∗ t₃ γ ∗ s₃ γ.
  Proof.
    iIntros "H2 [H1 $]". iCombine "H1" "H2" as "HH".
    by replace ((1 / 2 ⋅ 1 / 2 / 2) + (1 / 2 / 2))%Qp with 1%Qp
      by (apply (bool_decide_unpack _); by compute).
  Qed.
  Lemma transfer_alloc : ⊢ |==> ∃ γ, t₁ γ ∗ s₁ γ ∗ t₂ γ.
  Proof.
    iMod (own_alloc (Excl ())) as (transfer_name₂) "Htok"; first done.
    iMod (own_alloc 1%Qp) as (transfer_name₁) "[Ht₁ [Hs₁ Ht₂]]"; first done.
    iExists {| transfer_name₁ := transfer_name₁; transfer_name₂ := transfer_name₂ |}. iFrame; auto.
  Qed.
End sts.
