(* This file shows that given a BI [PROP], the type of predicates over
non-negative fractions [fracPred PROP], is again form a BI. This BI is the key
to the abstract Iron++ logic in the paper.

Apart from showing that [fracPred PROP] is a BI, this file also shows that
it is a step-indexed BI (i.e. with the [▷] modality), and that various other
operators on BIs can be lifted, namely the basic update modality [|=>] and the
plain modality [■]. It does not lift the fancy update modality [|={E1,E2}=>]
because that requires the [perm] connective which is specific to the Iron logic
(i.e. [perm] is not a BI generic construct).

The file moreover defines type class instances for the embedding [ ⎡ ⎤ ] from
the underlying BI [PROP] into [fracPred PROP], and defines the class
[FObjective] of elements of [fracPred PROP] that is independent of the fraction.

As a technical detail: Since we only have the type [Qp] of positive fractions,
we use the type [option Qp] to represent non-negative fractions. *)
From iris.algebra Require Export frac.
From stdpp Require Import coPset.
From iris.bi Require Export bi.

Record fracPred (PROP : Type) := FracPred { fracPred_at :> option Qp → PROP }.
Arguments FracPred {_} _.
Arguments fracPred_at {_} _ _.

Section ofe.
  Context {PROP : ofe}.
  Inductive fracPred_equiv' (P Q : fracPred PROP) :=
    { fracPred_in_equiv π : P π ≡ Q π }.
  Instance fracPred_equiv : Equiv (fracPred PROP) := fracPred_equiv'.

  Inductive fracPred_dist' (n : nat) (P Q : fracPred PROP) :=
    { fracPred_in_dist π : P π ≡{n}≡ Q π }.
  Instance fracPred_dist : Dist (fracPred PROP) := fracPred_dist'.

  Lemma fracPred_ofe_mixin : OfeMixin (fracPred PROP).
  Proof.
    refine (iso_ofe_mixin (λ P : fracPred PROP, P : _ -d> _) _ _);
      (split; [by destruct 1|by constructor]).
  Qed.
  Canonical Structure fracPredO : ofe := Ofe (fracPred PROP) fracPred_ofe_mixin.

  Global Instance fracPred_cofe `{Cofe PROP} : Cofe fracPredO.
  Proof.
    refine (iso_cofe (λ P : _ -d> _, @FracPred PROP P) id _ _).
    - split; [by destruct 1|by constructor].
    - done.
  Qed.

  Global Instance fracPred_at_ne n : Proper (dist n ==> (=) ==> dist n) fracPred_at.
  Proof. by intros ?? [?] ?? ->. Qed.
  Global Instance fracPred_at_proper : Proper ((≡) ==> (=) ==> (≡)) fracPred_at.
  Proof. by intros ?? [?] ?? ->. Qed.
End ofe.

(** BI canonical structure *)
Section bi.
  Context {PROP : bi}.
  Notation fracPred := (fracPred PROP).

  Inductive fracPred_entails (P1 P2 : fracPred) : Prop :=
    { fracPred_in_entails π : P1 π ⊢ P2 π }.
  Hint Immediate fracPred_in_entails : core.

  Definition fracPred_embed_def (P : PROP) : fracPred := FracPred (λ _, P)%I.
  Definition fracPred_embed_aux : seal (@fracPred_embed_def). by eexists. Qed.
  Definition fracPred_embed : Embed PROP fracPred := fracPred_embed_aux.(unseal).
  Definition fracPred_embed_eq : @embed _ _ fracPred_embed = _ := fracPred_embed_aux.(seal_eq).

  Definition fracPred_emp_def : fracPred := FracPred (λ π, <affine> ⌜ π = ε ⌝)%I.
  Definition fracPred_emp_aux : seal (@fracPred_emp_def). by eexists. Qed.
  Definition fracPred_emp := fracPred_emp_aux.(unseal).
  Definition fracPred_emp_eq : @fracPred_emp = _ := fracPred_emp_aux.(seal_eq).

  Definition fracPred_pure_def (φ : Prop) : fracPred := FracPred (λ _, ⌜φ⌝)%I.
  Definition fracPred_pure_aux : seal (@fracPred_pure_def). by eexists. Qed.
  Definition fracPred_pure := fracPred_pure_aux.(unseal).
  Definition fracPred_pure_eq : @fracPred_pure = _ := fracPred_pure_aux.(seal_eq).

  Definition fracPred_and_def (P Q : fracPred) : fracPred := FracPred (λ π, P π ∧ Q π)%I.
  Definition fracPred_and_aux : seal (@fracPred_and_def). by eexists. Qed.
  Definition fracPred_and := fracPred_and_aux.(unseal).
  Definition fracPred_and_eq : @fracPred_and = _ := fracPred_and_aux.(seal_eq).

  Definition fracPred_or_def (P Q : fracPred) : fracPred := FracPred (λ π, P π ∨ Q π)%I.
  Definition fracPred_or_aux : seal (@fracPred_or_def). by eexists. Qed.
  Definition fracPred_or := fracPred_or_aux.(unseal).
  Definition fracPred_or_eq : @fracPred_or = _ := fracPred_or_aux.(seal_eq).

  Definition fracPred_impl_def (P Q : fracPred) : fracPred := FracPred (λ π, P π → Q π)%I.
  Definition fracPred_impl_aux : seal (@fracPred_impl_def). by eexists. Qed.
  Definition fracPred_impl := fracPred_impl_aux.(unseal).
  Definition fracPred_impl_eq : @fracPred_impl = _ := fracPred_impl_aux.(seal_eq).

  Definition fracPred_forall_def {A} (Φ : A → fracPred) : fracPred :=
    FracPred (λ π, ∀ x : A, Φ x π)%I.
  Definition fracPred_forall_aux : seal (@fracPred_forall_def). by eexists. Qed.
  Definition fracPred_forall := fracPred_forall_aux.(unseal).
  Definition fracPred_forall_eq : @fracPred_forall = _ := fracPred_forall_aux.(seal_eq).

  Definition fracPred_exist_def {A} (Φ : A → fracPred) : fracPred :=
    FracPred (λ π, ∃ x : A, Φ x π)%I.
  Definition fracPred_exist_aux : seal (@fracPred_exist_def). by eexists. Qed.
  Definition fracPred_exist := fracPred_exist_aux.(unseal).
  Definition fracPred_exist_eq : @fracPred_exist = _ := fracPred_exist_aux.(seal_eq).

  Definition fracPred_sep_def (P Q : fracPred) : fracPred :=
    FracPred (λ π, ∃ π1 π2, ⌜ π = π1 ⋅ π2 ⌝ ∧ (P π1 ∗ Q π2))%I.
  Definition fracPred_sep_aux : seal (@fracPred_sep_def). by eexists. Qed.
  Definition fracPred_sep := fracPred_sep_aux.(unseal).
  Definition fracPred_sep_eq : @fracPred_sep = _ := fracPred_sep_aux.(seal_eq).

  Definition fracPred_wand_def (P Q : fracPred) : fracPred :=
    FracPred (λ π, ∀ π', P π' -∗ Q (π ⋅ π'))%I.
  Definition fracPred_wand_aux : seal (@fracPred_wand_def). by eexists. Qed.
  Definition fracPred_wand := fracPred_wand_aux.(unseal).
  Definition fracPred_wand_eq : @fracPred_wand = _ := fracPred_wand_aux.(seal_eq).

  Definition fracPred_persistently_def (P : fracPred) : fracPred :=
    FracPred (λ _, <pers> P ε)%I.
  Definition fracPred_persistently_aux : seal (@fracPred_persistently_def). by eexists. Qed.
  Definition fracPred_persistently := fracPred_persistently_aux.(unseal).
  Definition fracPred_persistently_eq : @fracPred_persistently = _ :=
    fracPred_persistently_aux.(seal_eq).

  Definition fracPred_later_def (P : fracPred) : fracPred := FracPred (λ π, ▷ P π)%I.
  Definition fracPred_later_aux : seal fracPred_later_def. by eexists. Qed.
  Definition fracPred_later := fracPred_later_aux.(unseal).
  Definition fracPred_later_eq : fracPred_later = _ := fracPred_later_aux.(seal_eq).
End bi.

Module Import fracPred.
  Definition unseal_eqs :=
    (@fracPred_and_eq, @fracPred_or_eq, @fracPred_impl_eq,
     @fracPred_forall_eq, @fracPred_exist_eq, @fracPred_sep_eq, @fracPred_wand_eq,
     @fracPred_persistently_eq, @fracPred_later_eq,
     @fracPred_embed_eq, @fracPred_emp_eq, @fracPred_pure_eq).
  Ltac unseal :=
    unfold bi_affinely, bi_absorbingly, bi_except_0, bi_pure, bi_emp, bi_and, bi_or,
           bi_impl, bi_forall, bi_exist, bi_sep, bi_wand,
           bi_persistently, bi_affinely, bi_later;
    simpl;
    rewrite !unseal_eqs /=.
End fracPred.

Lemma fracPred_bi_mixin (PROP : bi) :
  BiMixin (PROP:=fracPred PROP)
    fracPred_entails fracPred_emp fracPred_pure fracPred_and fracPred_or
    fracPred_impl fracPred_forall fracPred_exist fracPred_sep fracPred_wand.
Proof.
  split; try unseal; try by (split=> ? /=; repeat f_equiv).
  - split.
    + intros P. by split.
    + intros P Q R [H1] [H2]. split => ?. by rewrite H1 H2.
  - split.
    + intros [HPQ]. split; split => π; move: (HPQ π); by apply bi.equiv_entails.
    + intros [[] []]. split=>π. by apply bi.equiv_entails.
  - intros P φ ?. split=> π /=. by apply bi.pure_intro.
  - intros φ P HP. split=> π. apply bi.pure_elim'=> ?. by apply HP.
  - intros P Q. split=> π. by apply bi.and_elim_l.
  - intros P Q. split=> π. by apply bi.and_elim_r.
  - intros P Q R [?] [?]. split=> π. by apply bi.and_intro.
  - intros P Q. split=> π. by apply bi.or_intro_l.
  - intros P Q. split=> π. by apply bi.or_intro_r.
  - intros P Q R [?] [?]. split=> π. by apply bi.or_elim.
  - intros P Q R [HR]. split=> π /=. apply bi.impl_intro_r, HR.
  - intros P Q R [HR]. split=> π /=. by rewrite HR bi.impl_elim_l.
  - intros A P Ψ HΨ. split=> π. apply bi.forall_intro => ?. by apply HΨ.
  - intros A Ψ. split=> π. by apply: bi.forall_elim.
  - intros A Ψ a. split=> π. by rewrite /= -bi.exist_intro.
  - intros A Ψ Q HΨ. split=> π. apply bi.exist_elim => a. by apply HΨ.
  - intros P P' Q Q' [?] [?]. split=> π /=. apply bi.exist_mono=> π1.
    apply bi.exist_mono=> π2. by apply bi.and_mono_r, bi.sep_mono.
  - intros P. split=> π /=. rewrite -(bi.exist_intro None) -(bi.exist_intro π).
    by rewrite -bi.persistent_and_affinely_sep_l !bi.pure_True ?left_id_L // !left_id.
  - intros P. split=> π /=. apply bi.exist_elim=> π1. apply bi.exist_elim=> π2.
    apply bi.pure_elim_l=> ->. rewrite -bi.persistent_and_affinely_sep_l.
    apply bi.pure_elim_l=> ->. by rewrite left_id_L.
  - intros P Q. split=> π /=. apply bi.exist_elim=> π1. apply bi.exist_elim=> π2.
    by rewrite -(bi.exist_intro π2) -(bi.exist_intro π1) comm_L (comm bi_sep).
  - intros P Q R. split=> π /=. apply bi.exist_elim=> π'. apply bi.exist_elim=> π3.
    apply bi.pure_elim_l=> ->. repeat setoid_rewrite bi.sep_exist_r.
    apply bi.exist_elim=> π1. apply bi.exist_elim=> π2.
    rewrite bi.persistent_and_affinely_sep_l -assoc.
    rewrite -bi.persistent_and_affinely_sep_l. apply bi.pure_elim_l=> ->.
    rewrite -(bi.exist_intro π1) -(bi.exist_intro (π2 ⋅ π3)) assoc_L.
    rewrite bi.pure_True // left_id -(bi.exist_intro π2) -(bi.exist_intro π3).
    by rewrite bi.pure_True // left_id assoc.
  - intros P Q R [HR]. split=> π /=. apply bi.forall_intro=> π'.
    apply bi.wand_intro_r.
    by rewrite -HR /= -!bi.exist_intro bi.pure_True // left_id.
  - intros P Q R [HP]. split=> π /=. apply bi.exist_elim=> π1.
    apply bi.exist_elim=> π2. apply bi.pure_elim_l=> ->.
    by rewrite HP /= (bi.forall_elim π2) bi.wand_elim_l.
Qed.

Lemma fracPred_bi_persistently_mixin (PROP : bi) :
  BiPersistentlyMixin (PROP:=fracPred PROP)
    fracPred_entails fracPred_emp fracPred_and
    fracPred_exist fracPred_sep fracPred_persistently.
Proof.
  split; try unseal; try by (split=> ? /=; repeat f_equiv).
  - intros P Q [?]. split=> π /=. by f_equiv.
  - intros P. split=> π. by apply bi.persistently_idemp_2.
  - split=> π /=. apply (bi.persistently_intro _ _), (bi.affinely_intro _ _).
    by apply bi.pure_intro.
  - intros A Ψ. split=> π /=. by apply bi.persistently_and_2.
  - intros A Ψ. split=> π /=. by apply bi.persistently_exist_1.
  - intros P Q. split=> π /=. apply bi.exist_elim=> π1. apply bi.exist_elim=> π2.
    apply bi.pure_elim_l=> _. by rewrite bi.sep_elim_l.
  - intros P Q. split=> π /=. rewrite -(bi.exist_intro ε) -(bi.exist_intro π).
    rewrite left_id_L bi.pure_True // left_id. by apply bi.persistently_and_sep_elim.
Qed.

Lemma fracPred_bi_later_mixin (PROP : bi) :
  BiLaterMixin (PROP:=fracPred PROP)
    fracPred_entails fracPred_pure fracPred_or fracPred_impl
    fracPred_forall fracPred_exist fracPred_sep
    fracPred_persistently fracPred_later.
Proof.
  split; unseal.
  - by split=> ? /=; repeat f_equiv.
  - intros P Q [?]. split=> π. by apply bi.later_mono.
  - intros P. split=> π /=. by apply bi.later_intro.
  - intros A Ψ. split=> π. by apply bi.later_forall_2.
  - intros A Ψ. split=> π. simpl. by apply bi.later_exist_false.
  - intros P Q. split=> π /=. repeat setoid_rewrite bi.later_exist.
    apply bi.exist_elim=> π1. apply bi.exist_elim=> π2.
    rewrite bi.later_and (timeless ⌜ _ ⌝%I) /bi_except_0 bi.and_or_r.
    apply bi.or_elim.
    { rewrite bi.and_elim_l -(bi.exist_intro π) -(bi.exist_intro ε).
      rewrite right_id_L -bi.later_sep.
      auto using bi.and_intro, bi.False_elim, bi.later_mono, bi.pure_intro. }
    by rewrite -(bi.exist_intro π1) -(bi.exist_intro π2) bi.later_sep.
  - intros P Q. split=> π /=. repeat setoid_rewrite bi.later_exist.
    apply bi.exist_mono=> π1; apply bi.exist_mono=> π2.
    by rewrite {1}(bi.later_intro ⌜ _ ⌝)%I -bi.later_sep -bi.later_and.
  - intros P. split=> π. by apply bi.later_persistently_1.
  - intros P. split=> π. by apply bi.later_persistently_2.
  - intros P. split=> π /=. apply bi.later_false_em.
Qed.

Canonical Structure fracPredI (PROP : bi) : bi :=
  {| bi_ofe_mixin := fracPred_ofe_mixin;
     bi_bi_mixin := fracPred_bi_mixin PROP;
     bi_bi_persistently_mixin := fracPred_bi_persistently_mixin PROP;
     bi_bi_later_mixin := fracPred_bi_later_mixin PROP |}.

Class FObjective {PROP : bi} (P : fracPred PROP) :=
  fobjective_at π π' : P π ⊢ P π'.
Arguments FObjective {_} _%_I.
Arguments fobjective_at {_} _%_I {_}.
Global Hint Mode FObjective + ! : typeclass_instances.
Global Instance: Params (@FObjective) 1 := {}.

(** Primitive facts that cannot be deduced from the BI structure. *)
Section bi_facts.
  Context {PROP : bi}.
  Local Notation fracPred := (fracPred PROP).
  Local Notation fracPredI := (fracPredI PROP).
  Local Notation fracPred_at := (@fracPred_at PROP).
  Implicit Types P Q : fracPred.

  (** Instances *)
  Global Instance fracPred_bi_pure_forall `{!BiPureForall PROP} :
    BiPureForall fracPredI.
  Proof. intros A φ. split=> π. unseal. apply pure_forall_2. Qed.
  Global Instance fracPred_at_mono : Proper ((⊢) ==> (=) ==> (⊢)) fracPred_at.
  Proof. by move=> ?? [?] ?? ->. Qed.
  Global Instance fracPred_at_flip_mono :
    Proper (flip (⊢) ==> (=) ==> flip (⊢)) fracPred_at.
  Proof. solve_proper. Qed.

  Global Instance fracPred_later_contractive :
    Contractive (bi_later (PROP:=PROP)) → Contractive (bi_later (PROP:=fracPredI)).
  Proof. unseal=> ? n P Q HPQ. split=> i /=. f_contractive. apply HPQ. Qed.
  Global Instance fracPred_bi_löb : BiLöb PROP → BiLöb fracPredI.
  Proof. rewrite {2}/BiLöb=> ? P. unseal=> H. split=> i. apply löb_weak, H. Qed.
  Global Instance fracPred_positive : BiPositive PROP → BiPositive fracPredI.
  Proof.
    split => π. unseal. rewrite bi.affinely_and_l -bi.affinely_and_r.
    apply bi.pure_elim_l=> ->. repeat setoid_rewrite bi.affinely_exist.
    apply bi.exist_mono=> π1; apply bi.exist_mono=> π2.
    rewrite -bi.affinely_and_r bi_positive (symmetry_iff (=)) op_None.
    apply bi.pure_elim_l=> -[-> ->]. rewrite bi.affinely_and_l -bi.affinely_and_r.
    auto using bi.and_intro, bi.sep_mono, bi.pure_intro.
  Qed.

  Lemma fracPred_objective_persistent P :
    FObjective P → Persistent (P ε) → Persistent P.
  Proof.
    rewrite /FObjective /Persistent=> HP HP'; split=> π. unseal.
    by rewrite (HP π ε) {1}HP'.
  Qed.

  Global Instance fracPred_at_persistent P : Persistent P → Persistent (P ε).
  Proof. move => [] /(_ ε). by unseal. Qed.
  Global Instance fracPred_at_absorbing P π : Absorbing P → Absorbing (P π).
  Proof.
    move => [] /(_ π). rewrite /Absorbing. unseal.
    by rewrite -(bi.exist_intro ε) -(bi.exist_intro π) left_id_L bi.pure_True // left_id.
  Qed.
  Global Instance fracPred_at_affine P π : Affine P → Affine (P π).
  Proof. move => [] /(_ π). rewrite /Affine. unseal. by rewrite bi.affinely_elim_emp. Qed.

  Definition fracPred_embedding_mixin : BiEmbedMixin PROP fracPredI fracPred_embed.
  Proof.
    split; try apply _; unfold bi_emp_valid; unseal; try done.
    - move => P [/(_ inhabitant) /= <-].
      by apply (bi.affinely_intro _ _), bi.pure_intro.
    - intros PROP' ? P Q.
      set (f P := fracPred_at P inhabitant).
      assert (NonExpansive f) by solve_proper.
      apply (f_equivI f).
    - split=> π /=. by apply bi.affinely_elim_emp.
    - split=> π /=. apply (anti_symm _).
      + by rewrite -(bi.exist_intro π) -(bi.exist_intro ε) right_id_L bi.pure_True // left_id.
      + apply bi.exist_elim=> π1. apply bi.exist_elim=> π2. by apply bi.pure_elim_l.
    - split=> π /=. by rewrite (bi.forall_elim ε).
  Qed.
  Global Instance fracPred_bi_embed : BiEmbed PROP fracPredI :=
    {| bi_embed_mixin := fracPred_embedding_mixin |}.

  (** fracPred_at unfolding laws *)
  Lemma fracPred_at_embed π (P : PROP) : fracPred_at ⎡P⎤ π ⊣⊢ P.
  Proof. by unseal. Qed.
  Lemma fracPred_at_pure π (φ : Prop) : fracPred_at ⌜φ⌝ π ⊣⊢ ⌜φ⌝.
  Proof. by unseal. Qed.
  Lemma fracPred_at_emp π : fracPred_at emp π ⊣⊢ <affine> ⌜ π = ε ⌝.
  Proof. by unseal. Qed.
  Lemma fracPred_at_and π P Q : (P ∧ Q) π ⊣⊢ P π ∧ Q π.
  Proof. by unseal. Qed.
  Lemma fracPred_at_or π P Q : (P ∨ Q) π ⊣⊢ P π ∨ Q π.
  Proof. by unseal. Qed.
  Lemma fracPred_at_impl π P Q : (P → Q) π ⊣⊢ (P π → Q π).
  Proof. by unseal. Qed.
  Lemma fracPred_at_forall {A} π (Φ : A → fracPred) : (∀ x, Φ x) π ⊣⊢ ∀ x, Φ x π.
  Proof. by unseal. Qed.
  Lemma fracPred_at_exist {A} π (Φ : A → fracPred) : (∃ x, Φ x) π ⊣⊢ ∃ x, Φ x π.
  Proof. by unseal. Qed.
  Lemma fracPred_at_sep π P Q : (P ∗ Q) π ⊣⊢ ∃ π1 π2, ⌜ π = π1 ⋅ π2 ⌝ ∧ (P π1 ∗ Q π2).
  Proof. by unseal. Qed.
  Lemma fracPred_at_sep_2 π1 π2 P Q : P π1 ∗ Q π2 ⊢ (P ∗ Q) (π1 ⋅ π2).
  Proof.
    rewrite fracPred_at_sep -(bi.exist_intro π1) -(bi.exist_intro π2).
    by rewrite bi.pure_True // left_id.
  Qed.
  Lemma fracPred_at_wand π P Q : (P -∗ Q) π ⊣⊢ ∀ π', P π' -∗ Q (π ⋅ π').
  Proof. by unseal. Qed.
  Lemma fracPred_at_persistently π P : (<pers> P) π ⊣⊢ <pers> (P ε).
  Proof. by unseal. Qed.
  Lemma fracPred_at_affinely π P : (<affine> P) π ⊣⊢ <affine> (⌜ π = ε ⌝ ∧ P ε).
  Proof.
    rewrite /bi_affinely fracPred_at_and fracPred_at_emp -assoc.
    rewrite bi.pure_alt !bi.and_exist_r !bi.and_exist_l.
    by apply bi.exist_proper=> ->.
  Qed.
  Lemma fracPred_at_intuitionistically π P : (□ P) π ⊣⊢ □ (⌜ π = ε ⌝ ∧ P ε).
  Proof.
    rewrite fracPred_at_affinely fracPred_at_persistently.
    by rewrite -{1}bi.persistently_pure -bi.persistently_and.
  Qed.
  Lemma fracPred_at_absorbingly π P : (<absorb> P) π ⊣⊢ <absorb> (∃ π', ⌜ π' ≼ π ⌝ ∧ P π').
  Proof.
    rewrite /bi_absorbingly fracPred_at_sep. apply (anti_symm _).
    - apply bi.exist_elim=> π1; apply bi.exist_elim=> π2; apply bi.pure_elim_l=> ->.
      rewrite -(bi.exist_intro π2) fracPred_at_pure.
      rewrite bi.pure_True; last apply: cmra_included_r. by rewrite left_id.
    - rewrite bi.sep_exist_l. apply bi.exist_elim=> π'.
      rewrite bi.persistent_and_affinely_sep_l assoc (comm _ True%I) -assoc.
      rewrite -bi.persistent_and_affinely_sep_l.
      apply bi.pure_elim_l=> -[π'' /leibniz_equiv_iff->].
      rewrite -(bi.exist_intro π'') -(bi.exist_intro π') comm_L bi.pure_True //.
      by rewrite left_id fracPred_at_pure.
  Qed.

  (** Objective *)
  Global Instance Objective_proper : Proper ((≡) ==> iff) (@FObjective PROP).
  Proof. rewrite /FObjective. intros P P' HP. by setoid_rewrite HP. Qed.
  Global Instance embed_objective (P : PROP) : @FObjective PROP ⎡P⎤.
  Proof. intros ??. by unseal. Qed.
  Global Instance pure_objective φ : @FObjective PROP ⌜φ⌝.
  Proof. intros ??. by unseal. Qed.

  Global Instance and_objective P Q : FObjective P → FObjective Q → FObjective (P ∧ Q).
  Proof. intros ?? π π'. unseal. by f_equiv. Qed.
  Global Instance or_objective P Q : FObjective P → FObjective Q → FObjective (P ∨ Q).
  Proof. intros ?? π π'. unseal. by f_equiv. Qed.
  Global Instance impl_objective P Q : FObjective P → FObjective Q → FObjective (P → Q).
  Proof. intros ?? π π'. unseal. f_equiv; by simpl. Qed.
  Global Instance forall_objective {A} (Φ : A → fracPred) :
    (∀ x : A, FObjective (Φ x)) → FObjective (∀ x, Φ x).
  Proof. intros H π π'. unseal. f_equiv=> x /=. apply H. Qed.
  Global Instance exist_objective {A} (Φ : A → fracPred) :
    (∀ x : A, FObjective (Φ x)) → FObjective (∃ x, Φ x).
  Proof. intros H π π'. unseal. f_equiv=> x /=. apply H. Qed.

  Global Instance sep_objective P Q : FObjective P → FObjective Q → FObjective (P ∗ Q).
  Proof.
    intros ?? π π'. unseal. apply bi.exist_elim=> π1; apply bi.exist_elim=> π2.
    rewrite -(bi.exist_intro π') -(bi.exist_intro ε) right_id_L.
    f_equiv; [auto using bi.pure_intro|by f_equiv].
  Qed.
  Global Instance wand_objective P Q : FObjective Q → FObjective (P -∗ Q).
  Proof. intros ? π π'. unseal. f_equiv=> π''. by f_equiv. Qed.

  Global Instance persistently_objective P : FObjective (<pers> P).
  Proof. intros π π'. by unseal. Qed.
  (** Not an instance, it will blow up instance search *)
  Lemma persistent_objective P : Absorbing P → Persistent P → FObjective P.
  Proof. intros ??. rewrite -(bi.persistent_persistently P). apply _. Qed.
  Global Instance absorbingly_objective P : FObjective P → FObjective (<absorb> P).
  Proof. rewrite /bi_absorbingly. apply _. Qed.
  Global Instance persistently_if_objective P p : FObjective P → FObjective (<pers>?p P).
  Proof. rewrite /bi_persistently_if. destruct p; apply _. Qed.

  (** Big op *)
  Global Instance fracPred_at_monoid_and_homomorphism π :
    MonoidHomomorphism bi_and bi_and (≡) (flip fracPred_at π).
  Proof. split; [split|]; try apply _. apply fracPred_at_and. apply fracPred_at_pure. Qed.
  Global Instance fracPred_at_monoid_or_homomorphism π :
    MonoidHomomorphism bi_or bi_or (≡) (flip fracPred_at π).
  Proof. split; [split|]; try apply _. apply fracPred_at_or. apply fracPred_at_pure. Qed.

  (** BUpd *)
  Definition fracPred_bupd_def `{BiBUpd PROP} (P : fracPred) : fracPred :=
    FracPred (λ π, |==> P π)%I.
  Definition fracPred_bupd_aux `{BiBUpd PROP} : seal fracPred_bupd_def. by eexists. Qed.
  Definition fracPred_bupd `{BiBUpd PROP} : BUpd _ := fracPred_bupd_aux.(unseal).
  Definition fracPred_bupd_eq `{BiBUpd PROP} : @bupd _ fracPred_bupd = _ :=
    fracPred_bupd_aux.(seal_eq).

  Lemma fracPred_bupd_mixin `{BiBUpd PROP} : BiBUpdMixin fracPredI fracPred_bupd.
  Proof.
    split; rewrite fracPred_bupd_eq.
    - split=>/= π. solve_proper.
    - intros P. split=>/= π. apply bupd_intro.
    - intros P Q HPQ. split=>/= π. by rewrite HPQ.
    - intros P. split=>/= π. apply bupd_trans.
    - intros P Q. split=> π /=. unseal.
      apply bi.exist_elim=> π1. apply bi.exist_elim=> π2. apply bi.pure_elim_l=> ->.
      rewrite -(bi.exist_intro π1) -(bi.exist_intro π2) bi.pure_True // left_id.
      apply bupd_frame_r.
  Qed.
  Global Instance fracPred_bi_bupd `{BiBUpd PROP} : BiBUpd fracPredI :=
    {| bi_bupd_mixin := fracPred_bupd_mixin |}.

  Lemma fracPred_at_bupd `{BiBUpd PROP} π P : (|==> P) π ⊣⊢ |==> P π.
  Proof. by rewrite fracPred_bupd_eq. Qed.

  Global Instance fracPred_bi_embed_bupd `{BiBUpd PROP} : BiEmbedBUpd PROP fracPredI.
  Proof. split=>π /=. by rewrite fracPred_at_bupd !fracPred_at_embed. Qed.

  Global Instance bupd_objective `{BiBUpd PROP} P :
    FObjective P → FObjective (|==> P).
  Proof. intros ? π π'. rewrite fracPred_bupd_eq /=. by f_equiv. Qed.

  Lemma fracPred_affinely_bupd `{BiBUpd PROP, BiAffine PROP} P :
    (<affine> |==> P) ⊢ |==> <affine> P.
  Proof.
    split=> π. rewrite fracPred_at_affinely !fracPred_at_bupd fracPred_at_affinely.
    rewrite !bi.affine_affinely. apply bi.pure_elim_l=> ->.
    by rewrite bi.pure_True // left_id. 
  Qed.

  (** Later *)
  Global Instance fracPred_bi_embed_later : BiEmbedLater PROP fracPredI.
  Proof. split; by unseal. Qed.

  Lemma fracPred_at_later π P : (▷ P) π ⊣⊢ ▷ P π.
  Proof. by unseal. Qed.
  Lemma fracPred_at_except_0 π P : (◇ P) π ⊣⊢ ◇ P π.
  Proof. by unseal. Qed.

  Lemma fracPred_at_timeless_alt P : Timeless P ↔ ∀ π, Timeless (P π).
  Proof.
    rewrite /Timeless; split.
    - intros [HP] π. by rewrite -fracPred_at_later -fracPred_at_except_0.
    - intros HP; split=> π. by rewrite fracPred_at_later fracPred_at_except_0.
  Qed.
  Global Instance fracPred_emp_timeless :
    Timeless (@bi_emp PROP) → Timeless (@bi_emp fracPredI).
  Proof. intros. apply fracPred_at_timeless_alt. unseal. apply _. Qed.
  Global Instance fracPred_at_timeless P π : Timeless P → Timeless (P π).
  Proof. rewrite fracPred_at_timeless_alt. auto. Qed.

  Global Instance later_objective P : FObjective P → FObjective (▷ P).
  Proof. intros ? π π'. unseal. by f_equiv. Qed.
  Global Instance laterN_objective P n : FObjective P → FObjective (▷^n P).
  Proof. induction n; apply _. Qed.
  Global Instance except0_objective P : FObjective P → FObjective (◇ P).
  Proof. rewrite /bi_except_0. apply _. Qed.

  (** Internal equality *)
  Definition fracPred_internal_eq_def `{!BiInternalEq PROP} (A : ofe) (a b : A) : fracPred :=
    FracPred (λ _, a ≡ b)%I.
  Definition fracPred_internal_eq_aux `{!BiInternalEq PROP} : seal (@fracPred_internal_eq_def _).
  Proof. by eexists. Qed.
  Definition fracPred_internal_eq `{!BiInternalEq PROP} := fracPred_internal_eq_aux.(unseal).
  Definition fracPred_internal_eq_eq `{!BiInternalEq PROP} :
    @internal_eq _ (@fracPred_internal_eq _) = _ := fracPred_internal_eq_aux.(seal_eq).

  Lemma fracPred_internal_eq_mixin `{!BiInternalEq PROP} :
    BiInternalEqMixin fracPredI (@fracPred_internal_eq _).
  Proof.
    split; rewrite fracPred_internal_eq_eq.
    - by split=> ? /=; repeat f_equiv.
    - intros A P a. split=> π. by apply internal_eq_refl.
    - intros A a b Ψ ?. split=> π /=. unseal.
      erewrite (internal_eq_rewrite _ _ (flip Ψ _)) => //=. solve_proper.
    - intros A1 A2 f g. split=> π. unseal. by apply fun_extI.
    - intros A P x y. split=> π. by apply sig_equivI_1.
    - intros A a b ?. split=> π. unseal. by apply discrete_eq_1.
    - intros A x y. split=> π. unseal. by apply later_equivI_1.
    - intros A x y. split=> π. unseal. by apply later_equivI_2.
  Qed.
  Global Instance fracPred_bi_internal_eq `{BiInternalEq PROP} : BiInternalEq fracPredI :=
  {| bi_internal_eq_mixin := fracPred_internal_eq_mixin |}.

  Global Instance fracPred_bi_embed_internal_eq `{BiInternalEq PROP} :
    BiEmbedInternalEq PROP fracPredI.
  Proof. split=> i. rewrite fracPred_internal_eq_eq. by unseal. Qed.

  Lemma fracPred_internal_eq_unfold `{!BiInternalEq PROP} :
    @internal_eq fracPredI _ = λ A x y, ⎡ x ≡ y ⎤%I.
  Proof. rewrite fracPred_internal_eq_eq. by unseal. Qed.

  Lemma fracPred_at_internal_eq `{!BiInternalEq PROP} {A : ofe} π (a b : A) :
    @fracPred_at (a ≡ b) π ⊣⊢ a ≡ b.
  Proof. rewrite fracPred_internal_eq_unfold. by apply fracPred_at_embed. Qed.

  Lemma fracPred_equivI `{!BiInternalEq PROP'} P Q :
    P ≡ Q ⊣⊢@{PROP'} ∀ π, P π ≡ Q π.
  Proof.
    apply bi.equiv_entails. split.
    - apply bi.forall_intro=>?. apply (f_equivI (flip fracPred_at _)).
    - rewrite {2}(_ : P = FracPred P); last by destruct P.
      rewrite {2}(_ : Q = FracPred Q); last by destruct Q.
      by rewrite -(@f_equivI PROP' _ _ _ (@FracPred PROP : (_ -d> _) → fracPred)
        ltac:(by constructor)) -fun_extI.
  Qed.

  Global Instance internal_eq_objective `{!BiInternalEq PROP} {A : ofe} (x y : A) :
    @FObjective PROP (x ≡ y).
  Proof. intros ??. by rewrite !fracPred_at_internal_eq. Qed.

  (** Plainly *)
  Definition fracPred_plainly_def `{BiPlainly PROP} P : fracPred :=
    FracPred (λ _, ■ P ε)%I.
  Definition fracPred_plainly_aux `{BiPlainly PROP} : seal fracPred_plainly_def. by eexists. Qed.
  Definition fracPred_plainly `{BiPlainly PROP} : Plainly _ := fracPred_plainly_aux.(unseal).
  Definition fracPred_plainly_eq `{BiPlainly PROP} :
    @plainly _ fracPred_plainly = _ := fracPred_plainly_aux.(seal_eq).

  Lemma fracPred_plainly_mixin `{BiPlainly PROP} : BiPlainlyMixin fracPredI fracPred_plainly.
  Proof.
    split; rewrite fracPred_plainly_eq; try unseal.
    - by (split=> ? /=; repeat f_equiv).
    - intros P Q [?]. split=> π /=. by f_equiv.
    - intros P. split=> π /=. by rewrite plainly_elim_persistently.
    - intros P. split=> π /=. by rewrite -plainly_idemp_2.
    - intros A Ψ. split=> π /=. by rewrite plainly_forall.
    - intros P. split=> π /=. by rewrite plainly_impl_plainly.
    - intros P. split=> π /=. rewrite bi.pure_True // bi.affinely_True_emp.
      by rewrite -plainly_emp_intro.
    - intros P Q. split=> π /=. apply bi.exist_elim=> π1; apply bi.exist_elim=> π2.
      apply bi.pure_elim_l=> _. by rewrite bi.sep_elim_l.
    - intros P. split=> π /=. by rewrite -later_plainly_1.
    - intros P. split=> π /=. by rewrite -later_plainly_2.
  Qed.
  Global Instance fracPred_bi_plainly `{BiPlainly PROP} : BiPlainly fracPredI :=
    {| bi_plainly_mixin := fracPred_plainly_mixin |}.

  Lemma fracPred_at_plainly `{BiPlainly PROP} π P : (■ P) π ⊣⊢ ■ (P ε).
  Proof. by rewrite fracPred_plainly_eq. Qed.

  Global Instance fracPred_bi_plainly_exist `{BiPlainly PROP} :
    BiPlainlyExist PROP → BiPlainlyExist fracPredI.
  Proof.
    split=> π /=. rewrite fracPred_plainly_eq /= !fracPred_at_exist.
    by rewrite -plainly_exist_1.
  Qed.

  Global Instance fracPred_bi_prop_ext
    `{!BiPlainly PROP, !BiInternalEq PROP, !BiPropExt PROP} : BiPropExt fracPredI.
  Proof.
    intros P Q. split=> π /=. rewrite fracPred_equivI fracPred_at_forall.
    apply bi.forall_intro=> π'. rewrite fracPred_at_internal_eq fracPred_at_plainly.
    rewrite /bi_wand_iff fracPred_at_and !fracPred_at_wand.
    by rewrite prop_ext !(bi.forall_elim π') left_id_L.
  Qed.

  Global Instance fracPred_bi_embed_plainly `{BiPlainly PROP} :
    BiEmbedPlainly PROP fracPredI.
  Proof. intros P. by rewrite fracPred_plainly_eq; unseal. Qed.

  Global Instance fracPred_bi_persistently_forall `{BiPersistentlyForall PROP} :
    BiPersistentlyForall fracPredI.
  Proof. intros A φ. split=> /= i. unseal. by apply persistently_forall_2. Qed.

  Global Instance plainly_objective `{BiPlainly PROP} P : FObjective (■ P).
  Proof. intros π π'. by rewrite fracPred_plainly_eq. Qed.
End bi_facts.

Global Hint Immediate persistent_objective : typeclass_instances.
