(** This file proves very generic adequacy results for the lifted Iron logic.
These results are stated in a language independent way, so they are rather
daunting. For more concrete uses of this file, we recommend to take a look at
the [heap_lang/adequacy] file, where the results are used to show the absence
of memory leaks. *)
From iron.iron_logic Require Export weakestpre.
From iris.program_logic Require Export adequacy.
From iris.algebra Require Import lib.frac_auth ufrac.
From iris.proofmode Require Import proofmode.
Set Default Proof Using "Type".

(* Global functor setup *)
Definition ironInvΣ : gFunctors :=
  #[invΣ; cinvΣ; GFunctor (frac_authR ufracR)].

Class ironInvPreG (Σ : gFunctors) : Set := IronInvPreG {
  iron_inv_invPreG :: invGpreS Σ;
  iron_inv_cinvPreG :: cinvG Σ;
  fcinv_inPreG :: inG Σ (frac_authR ufracR);
}.

Global Instance subG_invΣ {Σ} : subG ironInvΣ Σ → ironInvPreG Σ.
Proof. solve_inG. Qed.

(* A generic version of basic adequacy: this applies when the main thread
has terminated. *)
Theorem iron_wp_adequacy Σ Λ `{ironInvPreG Σ} s e σ φ π π' :
  (∀ `{Hinv : invGS Σ} κs,
     ⊢ |={⊤}=> ∃
         (perm : frac → iProp Σ)
         (stateI : state Λ → list(observation Λ) → nat → iProp Σ)
         (fork_post : val Λ → iProp Σ)
         (_ : Fractional perm) (_ : ∀ π, Timeless (perm π)),
       let _ : ironG Λ Σ := IronG _ _ Hinv (IronInvG _ perm _ _ _ _) stateI fork_post in
       ∃ P : ironProp Σ,
       stateI σ κs 0 ∗ perm π ∗
       WP e @ s; ⊤ {{ v,
         P ∗
         <affine> ⎡ ∀ π1 π2 σ m, ⌜ π ⋅? π' = π1 ⋅? π2 ⌝ -∗
                                  stateI σ [] m -∗ perm π1 -∗ P π2 ={⊤,∅}=∗ ⌜φ v σ⌝ ⎤
       }} π') →
  adequate s e σ φ.
Proof.
  intros Hwp. apply adequate_alt; intros t2 σ2 [n [κs ?]]%erased_steps_nsteps.
  (* FIXME: This proof contains quite some duplication after we adapted it to
  later credits. Fix that. *)
  split.
  - intros v2 t2' ->.
    eapply (wp_strong_adequacy Σ _); [|done]=> ?.
    iMod Hwp as (perm stateI fork_post ?? P) "(Hσ&Hp&Hwp)". clear Hwp.
    iExists (λ σ _, stateI σ), [_], fork_post, _. iIntros "!> {$Hσ}".
    rewrite iron_wp_eq /=. iSplitL.
    { iSplitL; [|done]. iApply ("Hwp" with "Hp"). }
    iIntros ([|e2 []] t2'' ? Hlen Hsafe) "// Hσ [Hpost _] _"; simplify_eq/=.
    rewrite to_of_val /=.
    iDestruct "Hpost" as (π1 π2' ->) "[Hp H]". rewrite fracPred_at_sep.
    iDestruct "H" as (π2 π2b ->) "[HP H]". rewrite fracPred_at_affinely.
    iDestruct "H" as (->) "H". rewrite fracPred_at_embed bi.affinely_elim.
    iApply ("H" with "[] Hσ Hp HP"). by rewrite right_id_L.
  - intros e2 -> He2.
    eapply (wp_strong_adequacy Σ _); [|done]=> ?.
    iMod Hwp as (perm stateI fork_post ?? P) "(Hσ&Hp&Hwp)". clear Hwp.
    iExists (λ σ _, stateI σ), [_], fork_post, _. iIntros "!> {$Hσ}".
    rewrite iron_wp_eq /=. iSplitL.
    { iSplitL; [|done]. iApply ("Hwp" with "Hp"). }
    iIntros ([|e2' []] t2' ? Hlen Hsafe) "// Hσ [Hpost _] _"; simplify_eq/=.
    iApply fupd_mask_intro_discard; by auto.
Qed.

(* A generic version of adeqacy for correct usage of resources: this applies
when all threads have terminated. Contrary to basic adequacy, it provides the
postconditions of all forked-off threads [ [∗] Qs ] to establish the meta-level
post-condition [φ]. *)
Theorem iron_wp_all_adequacy Σ Λ `{ironInvPreG Σ} s e σ1 σ2 v vs φ π π' :
  (∀ `{Hinv : invGS Σ} κs,
     ⊢ |={⊤}=> ∃
         (perm : frac → iProp Σ)
         (stateI : state Λ → list(observation Λ) → nat → iProp Σ)
         (fork_post : val Λ → iProp Σ)
         (_ : Fractional perm) (_ : ∀ π, Timeless (perm π)),
       let _ : ironG Λ Σ := IronG _ _ Hinv (IronInvG _ perm _ _ _ _) stateI fork_post in
       ∃ P : ironProp Σ,
       stateI σ1 κs 0 ∗ perm π ∗
       WP e @ s; ⊤ {{ v,
         P ∗
         <affine> ⎡ ∀ π1 π2, ⌜ π ⋅? π' = π1 ⋅? π2 ⌝ -∗
                             stateI σ2 [] (length vs) -∗
                             ([∗ list] v ∈ vs, fork_post v) -∗
                             perm π1 -∗ P π2 ={⊤,∅}=∗ ⌜φ v⌝ ⎤
       }} π') →
  rtc erased_step ([e], σ1) (of_val <$> v :: vs, σ2) →
  φ v.
Proof.
  assert (∀ vs : list (val Λ), omap to_val (of_val <$> vs) = vs) as Hmap_val.
  { intros vs'. induction vs'; csimpl; rewrite ?to_of_val; auto with f_equal. }
  intros Hwp [n [κs Hsteps]]%erased_steps_nsteps.
  eapply (wp_strong_adequacy Σ _); [|done]=> ?.
  iMod Hwp as (perm stateI fork_post ?? P) "(Hσ&Hp&Hwp)".
  iExists (λ σ _, stateI σ), [_], fork_post, _. iIntros "!> {$Hσ}"; csimpl.
  rewrite iron_wp_eq /=. iSplitL.
  { iSplitL; [|done]. iApply ("Hwp" with "Hp"). }
  iIntros ([|e2 []] t2' ???) "// Hσ [Hpost _]"; simplify_eq/=.
  rewrite length_fmap to_of_val Hmap_val.
  iDestruct "Hpost" as (π1 π2' ->) "[Hp H]". rewrite fracPred_at_sep.
  iDestruct "H" as (π2 π2b ->) "[HP H]". rewrite fracPred_at_affinely.
  iDestruct "H" as (->) "H". rewrite fracPred_at_embed bi.affinely_elim.
  iIntros "HQs". iApply ("H" with "[//] Hσ HQs Hp"). by rewrite right_id_L.
Qed.
